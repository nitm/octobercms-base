<?php

namespace Nitm\Utils\Classes;

/**
 * This class provides configuration helper functions for config variables.
 *
 * @author malcolm@bklyn.co
 */
class ModelHelper
{
    /**
     * Get the tables based on the database driver.
     *
     * @param string $key The value to get from the config
     * @param string $db  The name of the database
     *
     * @return [type] [description]
     */
    public static function getIs($model)
    {
        if (method_exists($model, 'getMorphClass') && $model->getMorphClass()) {
            $parts = explode('\\', $model->getMorphClass());
        } else {
            $parts = explode('\\', get_class($model));
        }
        $str = array_pop($parts);
        preg_match_all('/((?:^|[A-Z])[a-z]+)/', $str, $matches);

        return strtolower(implode($matches[0], '-'));
    }

    /**
     * Get the id for a model.
     *
     * @method resolveId
     *
     * @param [type] $type [description]
     * @param [type] $id   [description]
     *
     * @return [type] [description]
     */
    public static function resolveId($type, $id, $namespace)
    {
        if (is_numeric($id)) {
            return $id;
        }
        $class = $namespace.'\\'.studly_case($type);
        if (class_exists($class)) {
            $existing = $class::apiFind($id);
            if (!$existing) {
                $parts = explode('-', $id);
                $hashedId = array_pop($parts);
                $existing = $class::apiFind($hashedId, ['hashedId' => true]);
                if (!($existing instanceof $class)) {
                    $existing = $class::apiFind(implode('-', $parts)) !== null;
                }
            }

            if ($existing) {
                return $existing->id;
            }

            return null;
        }
    }
}
