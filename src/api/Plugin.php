<?php

namespace Nitm\Api;

use App;
use Event;
use Cache;
use Config;
use Schema;
use Backend\Facades\Backend;
use System\Classes\PluginBase;
use System\Classes\SettingsManager;
use Illuminate\Foundation\AliasLoader;
use Nitm\Api\Models\Logs;
use Nitm\Api\Models\Eventlog;
use Nitm\Api\Models\Configs as RestfulConfig;
use Nitm\Api\Classes\Rest;
use Nitm\Api\Classes\Trivet;

class Plugin extends PluginBase
{
    /**
     * Plugin dependencies.
     *
     * @var array
     */
    public $require = ['RainLab.User'];

    public function pluginDetails()
    {
        return [
            'name' => 'nitm.api::lang.app.name',
            'description' => 'nitm.api::lang.app.desc',
            'author' => 'Nitm',
            'icon' => 'icon-map-signs',
        ];
    }

    public function registerPermissions()
    {
        return [
            'nitm.api.settings' => [
                'tab' => trans('nitm.api::lang.app.name'),
                'label' => 'Access & Edit API Configurations',
            ],
            'nitm.api.mappings' => [
                'tab' => trans('nitm.api::lang.app.name'),
                'label' => 'Access Request Mapping',
            ],
            'nitm.api.logs' => [
                'tab' => trans('nitm.api::lang.app.name'),
                'label' => 'Access API Logs',
            ],
            'nitm.api.eventhandler' => [
                'tab' => trans('nitm.api::lang.app.name'),
                'label' => 'Access Event Automation',
            ],
            'nitm.api.eventlogs' => [
                'tab' => trans('nitm.api::lang.app.name'),
                'label' => 'Access API Events Logs',
            ],
            'nitm.api.blacklist' => [
                'tab' => trans('nitm.api::lang.app.name'),
                'label' => 'Access IP Blacklist',
            ],
            'nitm.api.tokens' => [
                'tab' => trans('nitm.api::lang.app.name'),
                'label' => 'Tokens',
            ],
        ];
    }

    public function registerSettings()
    {
        return [
            'config' => [
                'label' => 'nitm.api::lang.app.name',
                'description' => 'nitm.api::lang.app.setting_desc',
                'icon' => 'icon-map-signs',
                'class' => 'Nitm\Api\Models\Configs',
                'order' => 998,
                'category' => SettingsManager::CATEGORY_SYSTEM,
                'permissions' => ['nitm.api.settings'],
            ],
        ];
    }

    public function registerComponents()
    {
        return [
           'Nitm\Api\Components\Api' => 'apiAPI',
           'Nitm\Api\Components\Auth' => 'apiAuthAPI',
           'Nitm\Api\Components\SocialAccount' => 'apiSocialConnectAPI',
        ];
    }

    public function registerReportWidgets()
    {
        return [
            'Nitm\Api\ReportWidgets\RestfulStatistics' => [
                'context' => 'dashboard',
                'label' => 'nitm.api::lang.widgets.statistics.label',
            ],
            'Nitm\Api\ReportWidgets\RestfulLogs' => [
                'context' => 'dashboard',
                'label' => 'nitm.api::lang.widgets.logs.label',
            ],
        ];
    }

    public function registerNavigation()
    {
        return [
            'api' => [
                'label' => 'nitm.api::lang.app.menu_label',
                'url' => Backend::url('nitm/api/history'),
                'icon' => 'icon-map-signs',
                'permissions' => ['nitm.api.*'],
                'order' => 998,

                'sideMenu' => [
                    'mappings' => [
                        'label' => 'nitm.api::lang.mapping.page_title',
                        'icon' => 'icon-connectdevelop',
                        'url' => Backend::url('nitm/api/mappings'),
                        'permissions' => ['nitm.api.mappings'],
                    ],
                    'eventhandler' => [
                        'label' => 'nitm.api::lang.event_automation.title',
                        'icon' => 'icon-magic',
                        'url' => Backend::url('nitm/api/eventhandler'),
                        'permissions' => ['nitm.api.eventhandler'],
                    ],
                    'history' => [
                        'label' => 'nitm.api::lang.logs.page_title',
                        'icon' => 'icon-history',
                        'url' => Backend::url('nitm/api/history'),
                        'permissions' => ['nitm.api.logs'],
                    ],
                    'eventhistory' => [
                        'label' => 'nitm.api::lang.eventlogs.page_title',
                        'icon' => 'icon-bolt',
                        'url' => Backend::url('nitm/api/eventhistory'),
                        'permissions' => ['nitm.api.eventlogs'],
                    ],
                    'blacklist' => [
                        'label' => 'nitm.api::lang.blacklist.page_title',
                        'icon' => 'icon-ban',
                        'url' => Backend::url('nitm/api/blacklist'),
                        'permissions' => ['nitm.api.blacklist'],
                    ],
                    'tokens' => [
                        'label' => 'nitm.api::lang.tokens.page_title',
                        'icon' => 'icon-key',
                        'url' => Backend::url('nitm/api/tokens'),
                        'permissions' => ['nitm.api.tokens'],
                    ],
                ],
            ],
        ];
    }

    /**
     * The register() method is called immediately when the plugin is registered.
     */
    public function register()
    {
        $alias = AliasLoader::getInstance();
        $alias->alias('Api', 'Nitm\Api\Facades\Api');

        App::singleton('api.api', function () {
            return Rest::instance();
        });

        // Register clearlogs console command
        // TODO: This functionality is not finished yet
        $this->registerConsoleCommand('nitm.api-clearlogs', 'Nitm\Api\Console\ClearLogs');
    }

    /**
     * The boot() method is called right before a request is routed.
     */
    public function boot()
    {
        \App::singleton('user.auth', function () {
            return \Nitm\Api\Classes\AuthManager::instance();
        });
        // Flush the cache for the lack of any wrong results
        // No idea why the cache was manually flushed here. Understand for realtime API but this should be a setting :-/
      //   Cache::flush();
        if (App::environment() != 'testing') {

           // Checks and seeds settings for mismatches
           RestfulConfig::seedSettings();

           // Set timezone based on user's choice
           date_default_timezone_set(RestfulConfig::get('timezone'));
            Config::set('app.timezone', RestfulConfig::get('timezone'));

           // Used for measuring script time passed
           ini_set('precision', 8);

           // Check for logs which past more than 31 days (default) which after created
         //   Trivet::purgeExpiredLogs(RestfulConfig::get('purge_logs_after'));

           // Show number of logs
           $this->extendNavigation();

           // Add event handler hooks for models
           if (Schema::hasTable('nitm_api_eventlogs')) {
               Trivet::instance()->handleEvents();
           }
        }
        if (App::environment() != 'production') {
            trace_sql();
        }
    }

    /**
     * Extend "Logs History" and "Event Logs History" navigations.
     */
    protected function extendNavigation()
    {
        Event::listen('backend.menu.extendItems', function ($manager) {
            // API Logs menu
            $logsCount = Logs::count();
            if ($logsCount) {
                $manager->addSideMenuItems('Nitm.Restful', 'api', [
                    'history' => [
                        'counter' => $logsCount,
                    ],
                ]);
            }

            // Event Logs menu
            $eventlogsCount = Eventlog::count();
            if ($eventlogsCount) {
                $manager->addSideMenuItems('Nitm.Restful', 'api', [
                    'eventhistory' => [
                        'counter' => $eventlogsCount,
                    ],
                ]);
            }
        });
    }
}
