<?php namespace Nitm\Api\ReportWidgets;

use DB;
use Backend\Classes\ReportWidgetBase;


class RestfulEventlogs extends ReportWidgetBase
{
    public function render()
    {
        try {
            $this->loadData();
        } catch (\Exception $ex) {
            $this->vars['error'] = $ex->getMessage();
        }

        return $this->makePartial('widget');
    }

    public function defineProperties()
    {
        return [
            'title' => [
                'title'             => 'backend::lang.dashboard.widget_title_label',
                'default'           => 'nitm.api::lang.widgets.eventlogs.label',
                'type'              => 'string',
                'validationPattern' => '^.+$',
                'validationMessage' => 'backend::lang.dashboard.widget_title_error'
            ],
            'showlogamount' => [
                'title'             => 'nitm.api::lang.widgets.eventlogs.count',
                'default'           => 7,
                'type'              => 'dropdown',
                'options'           => array_combine(range(1, 25),range(1, 25))
            ],
            'showmethod' => [
                'title'             => 'nitm.api::lang.widgets.eventlogs.method',
                'default'           => 'BOTH',
                'type'              => 'dropdown',
                'options'           => [
                    'BOTH'  => trans('nitm.api::lang.widgets.eventlogs.method_both'),
                    'GET'   => trans('nitm.api::lang.widgets.eventlogs.method_get'),
                    'POST'  => trans('nitm.api::lang.widgets.eventlogs.method_post')
                ]
            ],
            'showsuccess' => [
                'title'             => 'nitm.api::lang.widgets.eventlogs.success',
                'default'           => 'ALL',
                'type'              => 'dropdown',
                'options'           => [
                    'ALL'           => trans('nitm.api::lang.widgets.eventlogs.success_all'),
                    'success'    => trans('nitm.api::lang.widgets.eventlogs.success_only'),
                    'unsuccess'     => trans('nitm.api::lang.widgets.eventlogs.success_not')
                ]
            ],
            'showtimespent' => [
                'title'             => 'nitm.api::lang.widgets.eventlogs.timespent',
                'default'           => 'all',
                'type'              => 'dropdown',
                'options'           => [
                    '1'     => trans('nitm.api::lang.widgets.eventlogs.timespent_bt1s'),
                    '2'     => trans('nitm.api::lang.widgets.eventlogs.timespent_bt2s'),
                    '5'     => trans('nitm.api::lang.widgets.eventlogs.timespent_bt5s'),
                    '10'    => trans('nitm.api::lang.widgets.eventlogs.timespent_bt10s'),
                    'all'   => trans('nitm.api::lang.widgets.eventlogs.timespent_all')
                ]
            ],
        ];
    }

    protected function loadData()
    {
        /* Add where for request_method */
        $eventlogsMethodSelector = '%T';

        if ($this->property('showmethod') <> 'BOTH')
            $eventlogsMethodSelector = $this->property('showmethod');

        /* Add where for request status */
        if ($this->property('showsuccess') == 'ALL') {
            $eventlogsStatusSelector = "0";
            $eventlogsStatusOperator = ">";
        } elseif($this->property('showsuccess') == 'success') {
            $eventlogsStatusSelector = "200";
            $eventlogsStatusOperator = "=";
        } else {
            $eventlogsStatusSelector = "200";
            $eventlogsStatusOperator = "!=";
        }

        /* Add where for timepassed */
        $eventlogsTimeSpentSelector = 0;
        if ($this->property('showtimespent') <> 'all')
            $eventlogsTimeSpentSelector = $this->property('showtimespent');

        $this->vars['all_eventlogs'] = DB::table('nitm_api_eventlogs')
            ->where('request_method', 'like', $eventlogsMethodSelector)
            ->where('status_code', $eventlogsStatusOperator, $eventlogsStatusSelector)
            ->whereBetween('timepassed', [$eventlogsTimeSpentSelector, 9999])
            ->orderBy('created_at', 'desc')
            ->skip(0)
            ->take($this->property('showlogamount'))
            ->get();
    }
}
