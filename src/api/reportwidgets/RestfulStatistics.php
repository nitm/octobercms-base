<?php namespace Nitm\Api\ReportWidgets;

use DB;
use Backend\Classes\ReportWidgetBase;

class RestfulStatistics extends ReportWidgetBase
{
    public function render()
    {
        try {
            $this->loadData();
        }
        catch (\Exception $ex) {
            $this->vars['error'] = $ex->getMessage();
        }

        return $this->makePartial('widget');
    }

    public function defineProperties()
    {
        return [
            'title' => [
                'title'             => 'backend::lang.dashboard.widget_title_label',
                'default'           => 'nitm.api::lang.widgets.statistics.label',
                'type'              => 'string',
                'validationPattern' => '^.+$',
                'validationMessage' => 'backend::lang.dashboard.widget_title_error'
            ],
            'show_totals' => [
                'title'             => 'nitm.api::lang.widgets.statistics.show_totals',
                'default'           => true,
                'type'              => 'checkbox'
            ],
            'show_details' => [
                'title'             => 'nitm.api::lang.widgets.statistics.show_details',
                'default'           => true,
                'type'              => 'checkbox'
            ],
        ];
    }

    protected function loadData()
    {
        $this->vars['relationcount']            = DB::table('nitm_api_mappings')->count();
        $this->vars['logcount']                 = DB::table('nitm_api_logs')->count();
        $this->vars['status_ok']                = DB::table('nitm_api_logs')->where('status_code', 200)->count();
        $this->vars['status_failed']            = DB::table('nitm_api_logs')->where('status_code', 400)->count();
        $this->vars['status_blocked_ip']        = DB::table('nitm_api_logs')->where('status_code', 403)->count();
        $this->vars['status_method_fail']       = DB::table('nitm_api_logs')->where('status_code', 405)->count();
        $this->vars['status_param_not_match']   = DB::table('nitm_api_logs')->where('status_code', 420)->count();
        $this->vars['status_closed']            = DB::table('nitm_api_logs')->where('status_code', 503)->count();
        $this->vars['method_get']               = DB::table('nitm_api_logs')->where('request_method', 'GET')->count();
        $this->vars['method_post']              = DB::table('nitm_api_logs')->where('request_method', 'POST')->count();
        $this->vars['all_logs'] = ['title' => 'All Logs'];
    }
}
