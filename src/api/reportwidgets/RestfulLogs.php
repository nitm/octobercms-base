<?php namespace Nitm\Api\ReportWidgets;

use DB;
use Backend\Classes\ReportWidgetBase;

class RestfulLogs extends ReportWidgetBase
{
    public function render()
    {
        try {
            $this->loadData();
        } catch (\Exception $ex) {
            $this->vars['error'] = $ex->getMessage();
        }

        return $this->makePartial('widget');
    }

    public function defineProperties()
    {
        return [
            'title' => [
                'title'             => 'backend::lang.dashboard.widget_title_label',
                'default'           => 'nitm.api::lang.widgets.logs.label',
                'type'              => 'string',
                'validationPattern' => '^.+$',
                'validationMessage' => 'backend::lang.dashboard.widget_title_error'
            ],
            'showlogamount' => [
                'title'             => 'nitm.api::lang.widgets.logs.count',
                'default'           => 7,
                'type'              => 'dropdown',
                'options'           => array_combine(range(1, 25),range(1, 25))
            ],
            'showmethod' => [
                'title'             => 'nitm.api::lang.widgets.logs.method',
                'default'           => 'BOTH',
                'type'              => 'dropdown',
                'options'           => [
                    'BOTH'  => trans('nitm.api::lang.widgets.logs.method_both'),
                    'GET'   => trans('nitm.api::lang.widgets.logs.method_get'),
                    'POST'  => trans('nitm.api::lang.widgets.logs.method_post')
                ]
            ],
            'showsuccess' => [
                'title'             => 'nitm.api::lang.widgets.logs.success',
                'default'           => 'ALL',
                'type'              => 'dropdown',
                'options'           => [
                    'ALL'           => trans('nitm.api::lang.widgets.logs.success_all'),
                    'success'    => trans('nitm.api::lang.widgets.logs.success_only'),
                    'unsuccess'     => trans('nitm.api::lang.widgets.logs.success_not')
                ]
            ],
            'showtimespent' => [
                'title'             => 'nitm.api::lang.widgets.logs.timespent',
                'default'           => 'all',
                'type'              => 'dropdown',
                'options'           => [
                    '1'     => trans('nitm.api::lang.widgets.logs.timespent_bt1s'),
                    '2'     => trans('nitm.api::lang.widgets.logs.timespent_bt2s'),
                    '5'     => trans('nitm.api::lang.widgets.logs.timespent_bt5s'),
                    '10'    => trans('nitm.api::lang.widgets.logs.timespent_bt10s'),
                    'all'   => trans('nitm.api::lang.widgets.logs.timespent_all')
                ]
            ],
        ];
    }

    protected function loadData()
    {
        /* Add where for request_method */
        $logsMethodSelector = '%T';

        if ($this->property('showmethod') <> 'BOTH')
            $logsMethodSelector = $this->property('showmethod');

        /* Add where for request status */
        if ($this->property('showsuccess') == 'ALL') {
            $logsStatusSelector = "0";
            $logsStatusOperator = ">";
        } elseif($this->property('showsuccess') == 'success') {
            $logsStatusSelector = "200";
            $logsStatusOperator = "=";
        } else {
            $logsStatusSelector = "200";
            $logsStatusOperator = "!=";
        }

        /* Add where for timepassed */
        $logsTimeSpentSelector = 0;
        if ($this->property('showtimespent') <> 'all')
            $logsTimeSpentSelector = $this->property('showtimespent');

        $this->vars['all_logs'] = DB::table('nitm_api_logs')
            ->where('request_method', 'like', $logsMethodSelector)
            ->where('status_code', $logsStatusOperator, $logsStatusSelector)
            ->whereBetween('timepassed', [$logsTimeSpentSelector, 9999])
            ->orderBy('created_at', 'desc')
            ->skip(0)
            ->take($this->property('showlogamount'))
            ->get();
    }
}
