<?php
/**
 * @SWG\Swagger(
 *     schemes={"https"},
 *     host="api.dev.octopusartworks.com",
 *     basePath="/v1",
 *     produces={"application/json"},
 *     consumes={"application/json"},
 *     @SWG\Info(
 *         version="1.0.0",
 *         title="Octopus Artworks",
 *         description="This is the Octopus Artworks API",
 *         termsOfService="http://octopusartworks.com/api/terms/",
 *         @SWG\Contact(
 *             email="developers@octopusartworks.com"
 *         )
 *     )
 * )
 */
