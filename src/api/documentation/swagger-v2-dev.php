<?php
/**
 * @SWG\Swagger(
 *     schemes={"https"},
 *     host="nitm.thinklabserver.com",
 *     basePath="/malcolm/api",
 *     produces={"application/json"},
 *     consumes={"application/json"},
 *     @SWG\Info(
 *         version="1",
 *         title="Octopus Artworks DEV",
 *         description="This is the Octopus Artworks DEVELOPMENT API",
 *         termsOfService="http://octopusartworks.com/api/terms/",
 *         @SWG\Contact(
 *             email="developers@octopusartworks.com"
 *         )
 *     )
 * )
 */
