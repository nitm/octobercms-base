<?php

namespace Nitm\Api;

use PluginTestCase as BasePluginTestCase;

abstract class PluginTestCase extends BasePluginTestCase
{
    protected $baseUrl;

    /** Taken from October CMS test suite **/
    protected function spoofPageCode()
    {
        // Spoof all the objects we need to make a page object
        $theme = Theme::load('test');
        $page = Page::load($theme, 'index.htm');
        $layout = Layout::load($theme, 'content.htm');
        $controller = new Controller($theme);
        $parser = new CodeParser($page);
        $pageObj = $parser->source($page, $layout, $controller);

        return $pageObj;
    }
}
