<?php

namespace Nitm\Api\Helpers;

class Cache
{
    /**
     * Get a models cached at the specified key location.
     *
     * @param string $cacheKey [description]
     *
     * @return mixed [description]
     */
    protected static function getCachedModels($cacheKey)
    {
        $data = self::get($cacheKey);
        $class = array_pull($data, 'class');
        if (!is_null($class) && class_exists($class)) {
            $data = \October\Rain\Database\Collection::make(static::populateModels($class, $data['data']));
        } else {
            $data = array_get($data, 'data', $data);
        }

        return $data;
    }

    /**
     * Get a model cached at the specified key location.
     *
     * @param string $cacheKey [description]
     *
     * @return mixed [description]
     */
    protected static function getCachedModel($cacheKey)
    {
        $data = self::get($cacheKey);
        $class = array_pull($data, 'class');
        if (!is_null($class) && class_exists($class)) {
            $data = static::populateModel($class, $data['data']);
        } else {
            $data = array_get($data, 'data', $data);
        }

        return $data;
    }

    protected static function remember($mapping, $duration, $callback, $many = false)
    {
        $cacheKey = Trivet::getCacheKey([$mapping->relatedtable]);
        $duration = $duration ?: \Config::get('octopus.apiCacheDuration');
        if (self::has($cacheKey)) {
            $data = self::get($cacheKey);
            $class = array_pull($data, 'class');
            if (!is_null($class) && class_exists($class)) {
                if ($many) {
                    $data = \October\Rain\Database\Collection::make(static::populateModels($class, $data['data']));
                } else {
                    $data = static::populateModel($class, $data['data']);
                }
            } else {
                $data = array_get($data, 'data', $data);
            }

            return $data;
        } else {
            $result = call_user_func_array($callback, [$mapping]);
            self::set($cacheKey, [
                'class' => $result['class'],
                'data' => $many ? static::modelsToArray($result['data']) : static::toArray($result['data']),
            ]);

            return $result['data'];
        }
    }

    /**
     * Create an objecct from an array of attributes.
     *
     * @param string $class      [description]
     * @param array  $attributes [description]
     *
     * @return $class Object
     */
    protected static function instantiate($class, $attributes)
    {
        return new $class($attributes);
    }

    /**
     * Convert an array of objects to an array of arrays.
     *
     * @param array | Collection $objects [description]
     *
     * @return array Converted obects
     */
    protected static function modelsToArray($objects)
    {
        $ret_val = [];
        foreach ($objects as $object) {
            $ret_val[] = static::toArray($object);
        }

        return $ret_val;
    }

    /**
     * Convert an object to an array.
     *
     * @param mixed|Model $object [description]
     *
     * @return array The array representation of the object
     */
    protected static function toArray($object)
    {
        if (is_array($object)) {
            return (array) $object;
        } else {
            return $object->toArray();
        }
    }
}
