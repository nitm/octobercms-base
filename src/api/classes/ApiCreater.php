<?php

namespace Nitm\Api\Classes;

use Carbon\Carbon;
use DB;
use Schema;
use Nitm\Api\Models\Configs as RestfulConfig;
use Illuminate\Database\QueryException;
use October\Rain\Exception\ApplicationException;

class ApiCreater
{
    use \Nitm\Api\Traits\ApiTrait;
    /**
     * Holds response data.
     */
    public $data;

    /**
     * Holds REST Class instance.
     */
    public $rest;

    /**
     * Holds columns for matching with database.
     */
    public $columns_to_operate = [];

    /**
     * Holds all relations.
     */
    public $all_relations = [];

    /**
     * Holds request parameters.
     */
    public $request_parameters = [];

    /**
     * Holds all fields.
     */
    public $fields = [];

    /**
     * Starter method of the component.
     */
    public function __construct()
    {
        $this->rest = Rest::instance();
        $this->type = Trivet::getInputs('req');
    }

    /**
     * Get all relations data.
     *
     * @param $fields
     * @param $request_parameters
     * @param $all_relations
     */
    public function passData($fields, $request_parameters, $all_relations)
    {
        $this->fields = $fields;
        $this->request_parameters = $request_parameters;
        $this->all_relations = $all_relations;
    }

    /**
     * Build column fields for insert.
     */
    public function buildColumnFields()
    {
        /* Build column fields for insert */
        foreach (Trivet::getInputs() as $key => $value) {
            if (!in_array($key, $this->fields['mandatory']) && !in_array($key, $this->fields['purge'])) {
                $this->columns_to_operate[$key] = $value;
            }
        }
    }

    /**
     * Check & fetch relation based data before direct table access control.
     */
    public function makeRelationBasedCreate()
    {
        /* Fetch Table columns for matching with parameters */
        $mapping = $this->all_relations[$this->type];
        if (class_exists(($class = $mapping->relatedtable))) {
            $class::setForApi(true);
            $model = new $class();
            if (!$model->can('create')) {
                throw new ApplicationException(trans('nitm.content::lang.errors.no_permission'), 403);
            }
            $this->saveModel($model);

            return;
        } else {
            $fieldsInDb = Trivet::getDbFields($mapping->relatedtable);
            $query = static::getQuery($mapping, 'Nitm\Content');

           /* Check for if any columns to operate defined */
           if (empty($this->columns_to_operate)) {
               Trivet::addApiLog(0, 400);
               throw new \Exception(trans('nitm.api::lang.responses.no_column_to_operate'), 400);
           }

           /* Probably "created_at" and "updated_at" not sent by parameters.. */
           $timeNow = Carbon::now();

            if (!isset($this->columns_to_operate['created_at'])) {
                $this->columns_to_operate['created_at'] = $timeNow;
            }

            if (!isset($this->columns_to_operate['updated_at'])) {
                $this->columns_to_operate['updated_at'] = $timeNow;
            }

            foreach ($this->columns_to_operate as $key => $value) {
                if (!in_array($key, $fieldsInDb)) {
                    unset($this->columns_to_operate[$key]);
                }
            }

            try {
                // "insertGetId" changed to "insert" for tables which not have any primary key
            $this->data = DB::table($this->all_relations[$this->type]->relatedtable)
                ->insert($this->columns_to_operate);
            } catch (QueryException $e) {
                Trivet::addApiLog(0, 400);
                throw new \Exception(trans('nitm.api::lang.responses.create_failed'), 400);
            } catch (\Exception $e) {
                Trivet::addApiLog(0, 400);
                throw new \Exception(trans('nitm.api::lang.responses.create_failed'), 400);
            }
        }
    }

    /**
     * Check if direct table access allowed.
     */
    public function makeDirectTableCreate()
    {
        /* Check if direct table access allowed */
        if (RestfulConfig::get('direct_table_output')) {
            /* If requested table exists */
            if (Schema::hasTable($this->type)) {
                /* Fetch Table columns for matching with parameters */
                $dbFields = Trivet::getDbFields();
                $fieldsInDb = [];

                foreach ($dbFields as $dbField) {
                    if ($dbField->TABLE_NAME == $this->type) {
                        $fieldsInDb[$dbField->COLUMN_NAME] = $dbField->COLUMN_NAME;
                    }
                }

                /* Check for if any columns to operate defined */
                if (empty($this->columns_to_operate)) {
                    Trivet::addApiLog(0, 400);
                    throw new \Exception(trans('nitm.api::lang.responses.no_column_to_operate'), 400);
                }

                /* Probably "created_at" and "updated_at" not sent by parameters.. */
                $timeNow = Carbon::now();

                if (!isset($this->columns_to_operate['created_at'])) {
                    $this->columns_to_operate['created_at'] = $timeNow;
                }

                if (!isset($this->columns_to_operate['updated_at'])) {
                    $this->columns_to_operate['updated_at'] = $timeNow;
                }

                foreach ($this->columns_to_operate as $key => $value) {
                    if (!in_array($key, $fieldsInDb)) {
                        unset($this->columns_to_operate[$key]);
                    }
                }

                /* Try.. */
                try {
                    // "insertGetId" changed to "insert" for tables which not have any primary key
                    $this->data = DB::table($this->type)
                        ->insert($this->columns_to_operate);
                } catch (QueryException $e) {
                    Trivet::addApiLog(0, 400);
                    throw new \Exception(trans('nitm.api::lang.responses.create_failed'), 400);
                } catch (\Exception $e) {
                    Trivet::addApiLog(0, 400);
                    throw new \Exception(trans('nitm.api::lang.responses.create_failed'), 400);
                }
            }
        } else {
            /* Check if request matches with any relation */
            if (!in_array($this->type, $this->request_parameters)) {
                Trivet::addApiLog(0, 400);
                throw new \Exception(trans('nitm.api::lang.responses.req_mismatch'), 400);
            }
        }
    }

    /**
     * Check for if update failed.
     */
    public function lastControl()
    {
        /* If update failed */
        if (!$this->data) {
            Trivet::addApiLog(0, 400);
            throw new \Exception(trans('nitm.api::lang.responses.create_failed'), 400);
        }

        return trans('nitm.api::lang.responses.create_ok', ['indexkey' => '#ID: '.$this->data]);
    }
}
