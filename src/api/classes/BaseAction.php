<?php

namespace Nitm\Api\Classes;

abstract class BaseAction
{
    use \Nitm\Api\Traits\ApiTrait;
    /**
     * Holds response data.
     */
    public $data;

    /**
     * Holds REST Class instance.
     */
    public $rest;

    /**
     * Holds columns for matching with database.
     */
    public $columns_to_operate = [];

    /**
     * Holds all relations.
     */
    public $all_relations = [];

    /**
     * Holds request parameters.
     */
    public $request_parameters = [];

    /**
     * Holds all fields.
     */
    public $fields = [];

    /**
     * Holds result limit value.
     */
    public $result_limit = 1000000;

    /**
     * Starter method of the component.
     */
    public function __construct()
    {
        $this->rest = Rest::instance();
        $this->type = Trivet::getInputs('req');
    }

    /**
     * Check for if update failed.
     */
    public function lastControl()
    {
        /* If update failed */
        if (!$this->data) {
            Trivet::addApiLog(0, 400);
            throw new \Exception(trans('nitm.api::lang.responses.create_failed'), 400);
        }

        return trans('nitm.api::lang.responses.create_ok', ['indexkey' => '#ID: '.$this->data]);
    }
}
