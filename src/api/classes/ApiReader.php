<?php

namespace Nitm\Api\Classes;

use DB;
use Schema;
use Nitm\Api\Models\Configs as RestfulConfig;
use October\Rain\Exception\ApplicationException;

class ApiReader
{
    use \Nitm\Api\Traits\ApiTrait;
    /**
     * Holds response data.
     */
    public $data;

    /**
     * Holds REST Class instance.
     */
    public $rest;

    /**
     * Holds all relations.
     */
    public $all_relations = [];

    /**
     * Holds request parameters.
     */
    public $request_parameters = [];

    /**
     * Holds index key.
     */
    public $where_index_key;

    /**
     * Holds fetch_by_key value
     * If index key sent as parameter, hold value for next queries.
     */
    public $fetch_by_key;

    /**
     * Holds operator value.
     */
    public $operator;

    /**
     * Holds result limit value.
     */
    public $result_limit = 1000000;

    /**
     * Starter method of the component.
     */
    public function __construct()
    {
        $this->rest = Rest::instance();
        $this->type = Trivet::getInputs('req');
    }

    /**
     * Get all relations data.
     *
     * @param $fields
     * @param $all_relations
     * @param $request_parameters
     * @param $where_index_key
     */
    public function passData($fields, $all_relations, $request_parameters, $where_index_key)
    {
        $this->all_relations = $all_relations;
        $this->request_parameters = $request_parameters;

        /* Check "Allow Requesting with Index Keys" option for relation */
        if (
            isset($this->all_relations[$this->type]) &&
            !$this->all_relations[$this->type]->allow_indexkeys
        ) {
            $this->fetch_by_key = Trivet::getInputs($where_index_key) != null ? Trivet::getInputs($where_index_key) : 0;
            $this->where_index_key = $where_index_key != null ? $where_index_key : 'id';
            $this->operator = Trivet::getInputs($where_index_key) != null ? '=' : '>=';
        } else {
            $inputs = Trivet::getInputs();

            if (!RestfulConfig::get('direct_table_output')) {
                $indexes = Trivet::getDbFields($this->all_relations[$this->type]->relatedtable);
            } else {
                $indexes = Trivet::getDbFields($this->type);
            }

            /* Clean inputs, unset mandatory, purge, optional fields */
            foreach ($inputs as $key => $value) {
                if (in_array($key, $fields['mandatory']) || in_array($key, $fields['purge']) || in_array($key, $fields['optional'])) {
                    unset($inputs[$key]);
                }
            }

            /* Look for if parameter is index key in DB */
            foreach ($indexes as $key => $value) {
                if (array_key_exists($value->getName(), $inputs)) {
                    $this->fetch_by_key = $inputs[$value->getName()];
                    $this->where_index_key = $value->getName();
                    $this->operator = '=';
                    /* If first occurrence is met, let's break it.. */
                    break;
                }
            }

            /* If there's no index key parameter defined, then give error */
            if (!isset($this->fetch_by_key) || !isset($this->where_index_key) || !isset($this->operator)) {
                Trivet::addApiLog(0, 400);

                throw new \Exception(trans('nitm.api::lang.responses.no_selector_key'), 400);
            }
        }
    }

    /**
     * If "result_limit" value entered bigger than 0 on relation basis..
     */
    public function defineResultLimit()
    {
        if (in_array($this->type, $this->request_parameters)) {
            if ($this->all_relations[$this->type]->result_limit > 0) {
                $this->result_limit = $this->all_relations[$this->type]->result_limit;
            }
        }
    }

    /**
     * Check & fetch relation based data before direct table access control.
     */
    public function makeRelationBasedRead()
    {
        $lists = $this->all_relations[$this->type]->responsefields;

        /* Maybe "id" field not present in database, so select first index key for order by */
        $mapping = $this->all_relations[$this->type];
        $this->data = \Nitm\Api\Helpers\Cache::remember(Trivet::getInputs(), $mapping, function ($mapping) {
            if (class_exists(($class = $mapping->relatedtable))) {
                $model = new $class();
                $class::setForApi(true);
                if (!$model->can('read')) {
                    throw new ApplicationException(trans('nitm.content::lang.errors.no_permission'), 403);
                }
               //  $model = $model->apiFind($this->fetch_by_key, ['hashedId' => true]);
               //  print_r($model->mediums()->toSql());
               //  print_r($model->mediums()->getBindings());
               //  exit;

                return $model->apiFind($this->fetch_by_key, ['hashedId' => true]);
            } else {
                //We're doing this like this
                $query = static::getQuery($mapping, 'Nitm\Content');
                if (!$query->getModel()->can('read')) {
                    throw new ApplicationException(trans('nitm.content::lang.errors.no_permission'), 403);
                }

                return [
                  '__cacheClass' => get_class($query->getModel()),
                  '__cacheData' => $query
                     ->addSelect($lists)
                     ->where($this->where_index_key, $this->operator, $this->fetch_by_key)
                     ->take(1)
                     ->get(),
               ];
            }
        }, config('octopus.cacheLimits.'.implode('-', \Route::current()->parameters())), $this->shouldRefresh($mapping));
    }

    /**
     * Check if direct table access allowed.
     */
    public function makeDirectTableRead()
    {
        if (RestfulConfig::get('direct_table_output')) {
            $mapping = new \stdClass();
            $mapping->relatedtable = $this->type;
            $this->data = \Cache::remember(Trivet::getInputs(), $mapping, function () use ($mapping) {
                /* If requested table exists */
               if (Schema::hasTable($mapping->relatedtable)) {
                   /* Maybe "id" field not present in database, so select first index key for order by */
                   $tableName = $mapping->relatedtable;
                   $index_keys = Trivet::getDbFields($tableName);

                   return [
                      '__cacheClass' => null,
                      '__cacheData' => DB::table($tableNam)
                       ->where($this->where_index_key, $this->operator, $this->fetch_by_key)
                       ->orderBy($index_keys[0]->getName())
                       ->get(),
                    ];
               }
            });
        } else {
            /* Check if request matches with any relation */
            if (!in_array($this->type, $this->request_parameters)) {
                Trivet::addApiLog(0, 400);

                throw new \Exception(trans('nitm.api::lang.responses.req_mismatch'), 400);
            }
        }
    }

    /**
     * Check for returned data from database.
     */
    public function lastControl()
    {
        /* If no data returned from database */
        if (!$this->data) {
            Trivet::addApiLog(0, 404);

            throw new \Exception(trans('nitm.api::lang.responses.no_data', [
                'indexkey' => '#'.$this->where_index_key.': '.$this->fetch_by_key,
            ]), 404);
        }

        return $this->data;
    }
}
