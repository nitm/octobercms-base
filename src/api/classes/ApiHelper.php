<?php

namespace Nitm\Api\Classes;

use Auth;
use Nitm\Api\Models\Configs as RestfulConfig;

class ApiHelper
{
    /**
     * Holds REST Class instance.
     */
    public $rest;

    protected $component;

    /**
     * Holds the Request instance.
     *
     * @var [type]
     */

    /**
     * Starter method of the component.
     */
    public function __construct($component)
    {
        $this->rest = Rest::instance();
        $this->component = $component;
    }

    /**
     * Helper method for Api component's "onRun".
     *
     * @param $apiStatus
     */
    public function checkIfApiActivated($apiStatus)
    {
        if (!RestfulConfig::get('api_status') || $apiStatus === 'off') {
            Trivet::addApiLog(0, 503);
            $this->rest->response(503, trans('nitm.api::lang.responses.maintenance'));
        }
    }

    /**
     * Helper method for Api component's "onRun".
     */
    public function checkIfScheduleStarted()
    {
        if (RestfulConfig::get('schedule_status')) {
            $current_time = date('Y-m-d H:i:s');
            $start = RestfulConfig::get('api_schedule_start');
            $end = RestfulConfig::get('api_schedule_end');

            if ($current_time > $start && $current_time < $end) {
                Trivet::addApiLog(0, 503);
                $this->rest->response(503, trans('nitm.api::lang.responses.maintenance'));
            }
        }
    }

    /**
     * Helper method for Api component's "onRun".
     */
    public function checkIfIpBlocked()
    {
        $ip = Ip::instance();

        $blacklistDb = RestfulConfig::get('ip_blacklist');
        $blacklist = explode("\r\n", $blacklistDb);

        foreach ($blacklist as $blackIP) {
            if (Ip::ipv4_between($ip->realIp(), $blackIP)) {
                Trivet::addApiLog(0, 403);
                $this->rest->response(403, trans('nitm.api::lang.responses.ip_blocked'));
            }
        }
    }

    /**
     * Helper method for Api component's "onRun"
     * Added check with user-credentials.
     */
    public function checkIfAuthCorrect()
    {
        $adminKey = RestfulConfig::get('admin_key');
        $authKeysDb = RestfulConfig::get('external_keys');
        $authKeys = explode("\r\n", $authKeysDb);

        $authWithUserCredentials = RestfulConfig::get('auth_with_user');
        $apiLogin = (Trivet::getInputs('api_login') ? Trivet::getInputs('api_login') : null);
        $apiPassword = (Trivet::getInputs('api_password') ? Trivet::getInputs('api_password') : null);

        // If auth should checked with user credentials
        if ($authWithUserCredentials) {
            // If auth checked with user credentials, also give permission to admin_key please..
            if ((!$apiLogin || !$apiPassword) && Trivet::getInputs('auth') != $adminKey && !in_array(Trivet::getInputs('auth'), $authKeys)) {
                Trivet::addApiLog(0, 400);
                $this->rest->response(400, trans('nitm.api::lang.responses.auth_mismatch', [
                    'params' => '(api_login) and (api_password)',
                ]));
            } else {
                if (Trivet::getInputs('auth') == $adminKey || in_array(Trivet::getInputs('auth'), $authKeys)) {
                    $this->component->controller->setIsAdmin(true);

                    return true;
                }

                try {
                    Auth::authenticate([
                        'login' => $apiLogin,
                        'password' => $apiPassword,
                    ], true);
                } catch (\Exception $e) {
                    Trivet::addApiLog(0, 400);
                    $this->rest->response(400, $e->getMessage());
                }
            }
        // If auth should checked with admin_key or external_keys
        } else {
            if (!Trivet::getInputs('auth') ||
                (Trivet::getInputs('auth') != $adminKey && !in_array(Trivet::getInputs('auth'), $authKeys))
            ) {
                $this->component->controller->setIsAdmin(true);
                Trivet::addApiLog(0, 400);
                $this->rest->response(400, trans('nitm.api::lang.responses.auth_mismatch', ['params' => '(auth)']));
            }
        }

        return true;
    }

    /**
     * Helper method for Api component's "onRun".
     *
     * @param $doOps
     *
     * @return mixed
     */
    public function checkIfCRUD($doOps)
    {
        $allInputs = Trivet::getInputs();

        /* Check if "do" parameter not matching */
        if (!isset($allInputs['do']) || !array_key_exists($allInputs['do'], $doOps)) {
            Trivet::addApiLog(0, 400);
            $this->rest->response(400, trans('nitm.api::lang.responses.do_mismatch'));
        }

        /* Then it's ok to return right method name for calling */
        return $doOps[$allInputs['do']];
    }

    /**
     * Helper method for Api component's "onRun".
     *
     * @param $doOps
     *
     * @return mixed
     */
    public function checkIfAuthCRUD($doOps)
    {
        $allInputs = Trivet::getInputs();

        /* Check if "do" parameter not matching */
        if (!isset($allInputs['action']) || !array_key_exists($allInputs['action'], $doOps)) {
            Trivet::addApiLog(0, 400);
            $this->rest->response(400, trans('nitm.api::lang.responses.action_mismatch'));
        }

        /* Then it's ok to return right method name for calling */
        return $doOps[$allInputs['action']];
    }
}
