<?php

namespace Nitm\Api\Classes;

use DB;
use Route;
use Illuminate\Support\Facades\Input;
use October\Rain\Support\Facades\Http;
use October\Rain\Support\Traits\Singleton;
use Schema;
use Request;
use Carbon\Carbon;
use Nitm\Content\Classes\DbHelper;
use Nitm\Api\Models\Logs;
use Nitm\Api\Models\Configs as RestfulConfig;
use Nitm\Api\Models\Eventlog;
use Nitm\Api\Models\Eventhandler;

class Trivet
{
    use Singleton;
    
    private $eventPostResponse = '';
    
    private $userAgentString = 'NITM RESTful_API/v1.0';

    public static function filterInput($data=[])
    {
        return array_filter(array_merge($data, \Input::all()), function ($value) {
            //Some support for filtering 'undefined' javascript values
             if ($value !== 'undefined') {
                 return true;
             }
        });
    }
    
    /**
    * Get Input values.
    *
    * @param null $oneOrAll
    * @param null $except
    *
    * @return array|mixed
    */
    public static function getInputs($oneOrAll = null, $except = null)
    {
        if (!Route::current()) {
            return ['test' => true];
        }
        if ($except) {
            return array_merge(array_except((array) static::getParameters(), (array) $except), Input::except($except));
        }
        
        if (!$oneOrAll) {
            $result = array_merge((array) Input::all(), (array) static::getParameters());
            
            return $result;
        }
        
        $allData = array_merge(Input::all(), static::getParameters());
        
        $result = (array) array_get($allData, $oneOrAll);
        
        return is_array($result) ? array_pop($result) : $result;
    }
    
    public static function getCacheKey()
    {
        return md5(json_encode(static::getInputs()));
    }
    
    /**
    * Add API Log to Database.
    *
    * @param int $timePassed
    * @param int $statusCode
    *
    * @return bool
    */
    public static function addApiLog($timePassed = 0, $statusCode = 200, $e = null)
    {
        if ($e) {
            switch ($statusCode) {
                case 500:
                    \System\Models\EventLog::add($e, 'critical');
                    break;
                
                case 200:
                    case 301:
                        case 302:
                            break;
                        
                        default:
                            \System\Models\EventLog::add($e, 'error');
                            break;
                }
        }
        try {
            /* Of course, if logging activated.. */
                if (RestfulConfig::get('api_logs')) {
                    
                    /* Save Log to DB */
                    $activity = new Logs();
                    
                    $time = date('Y-m-d H:i:s');
                    $activity->fill(array_merge([
                    'updated_at' => $time,
                    ], static::getCoreLogInfo(static::getInputs('do'))));
                    
                    return $activity->save();
                }
        } catch (\Exception $e) {
            throw $e;
        }
            
        return true;
    }
        
    /**
    * Add API Log to Database.
    *
    * @param int $timePassed
    * @param int $statusCode
    *
    * @return bool
    */
    public static function addViewLog($action, $options = [], $timePassed = 0, $statusCode = 200)
    {
        try {
            
            /* Save Log to DB */
            $activity = new \Nitm\Content\Models\ContentView();
            
            $activity->fill(array_merge($options, [
            'action' => $action,
            'duration' => $timePassed,
            'api_status' => RestfulConfig::get('api_status'),
            'cookie_hash' => md5(json_encode($_COOKIE)),
            ]));
            
            $result = $activity->save();
            
            return $result;
        } catch (\Exception $e) {
        }
        
        return true;
    }
    
    /**
    * Get the base log information we want to send.
    *
    * @param [type] $action     [description]
    * @param int    $statusCode [description]
    * @param int    $timePassed [description]
    *
    * @return [type] [description]
    */
    protected static function getCoreLogInfo($action, $statusCode = 200, $timePassed = 0)
    {
        return [
        'ip' => Ip::instance()->realIp(),
        'used_key' => json_encode(static::getApiToken()),
        'referer' => Request::server('HTTP_REFERER'),
        'browser' => Request::server('HTTP_USER_AGENT'),
        'fullurl' => static::fullUrl(),
        'request_method' => Request::server('REQUEST_METHOD'),
        'api_status' => RestfulConfig::get('api_status'),
        'timepassed' => $timePassed,
        'created_at' => date('Y-m-d H:i:s'),
        'status_code' => $statusCode,
        ];
    }
        
    public static function getApiToken()
    {
        $token = Trivet::getInputs('api_token') ?: array_get($_COOKIE, 'api_token');
        if (!$token) {
            $header = explode(' ', \Request::header('authorization'));
            $token = array_get($header, '1', null);
        }
        if (!$token) {
            $user = \Auth::getUser();
            if ($user && $user->apiToken) {
                $token = $user->apiToken->token;
            }
        }
            
        return $token ?: 'none';
    }
        
    public static function fullUrl()
    {
        if (Request::server('REQUEST_METHOD') == 'POST') {
            $fullUrl = Request::server('REQUEST_URI');
        } else {
            $fullUrl = Request::server('REQUEST_URI');
        }
            
        $urlParts = parse_url($fullUrl);
        $query = array_get($urlParts, 'query', null);
        if ($query) {
            parse_str($query, $args);
                
            $args = array_except($args, ['api_token']);
                
            return $urlParts['path'].'?'.http_build_query($args);
        } else {
            return $urlParts['path'];
        }
    }
        
    public static function getPluginModelClasses($namespace)
    {
        $namespaces = array_merge((array)$namespace, [
            'Nitm\\Content'
        ]);
        $classes = [];
        foreach ($namespaces as $ns) {
            $path = \System\Classes\PluginManager::instance()->getPluginPath($ns);
            $paths = array_map('basename', glob($path.'/models/*.php'));
            foreach ($paths as $file) {
                $modelName = basename($file, '.php');
                $classes[] = $ns.'\Models\\'.$modelName;
            }
        }
            
        return $classes;
    }
            
    public static function getModelClass($modelName, $namespace = null)
    {
        return array_get(static::getPluginModelClasses($namespace), ucfirst(strtolower($modelName)));
    }
            
    /**
    * Get the fields for a $target that couldbe either a table or a class.
    *
    * @param string $target The table or class we're using
    *
    * @return array The fields for $target
    */
    public static function getFieldsFromDbOrClass($target, $asArray = false)
    {
        if (class_exists($target)) {
            return static::getFieldsFromClass($target, $asArray);
        } else {
            return static::getDbFields($target, $asArray);
        }
    }
    
    /**
    * Get the fields for a $target that couldbe either a table or a class.
    *
    * @param string $target The table or class we're using
    *
    * @return array The fields for $target
    */
    public static function getIndexesFromDbOrClass($target, $asArray = false)
    {
        if (class_exists($target)) {
            return static::getIndexesFromClass($target, $asArray);
        } else {
            return static::getDbIndexes($target, $asArray);
        }
    }
            
    public static function getIndexesFromClass($className, $asArray = false)
    {
        $fields = [];
        if (class_exists($className)) {
            $model = new $className();
            $visible = $model->indexes;
            if ($visible) {
                foreach ($visible as $field) {
                    if ($asArray) {
                        $fields[$field] = [
                                'table_name' => $model->table,
                                'class_name' => $className,
                                'column_name' => $field,
                                'column_key' => $field,
                                ];
                    } else {
                        $fields[$field] = new \Doctrine\DBAL\Schema\Column($field, new \Doctrine\DBAL\Types\StringType());
                    }
                }
            }
        }
                
        return $fields;
    }
            
    public static function getFieldsFromClass($className, $asArray = false)
    {
        $fields = [];
        if (class_exists($className)) {
            $model = new $className();
            $visible = $model->visible;
            if ($visible) {
                foreach ($visible as $field) {
                    if ($asArray) {
                        $fields[$field] = [
                                'table_name' => $model->table,
                                'class_name' => $className,
                                'column_name' => $field,
                                'column_key' => '',
                                ];
                    } else {
                        $fields[$field] = new \Doctrine\DBAL\Schema\Column($field, new \Doctrine\DBAL\Types\StringType());
                    }
                }
            }
        }
                
        return $fields;
    }
            
    /**
    * @param string $tableName
    *
    * @return array
    */
    public static function getDbFields($tableName, $asArray = false)
    {
        if (class_exists($tableName)) {
            $tableName = (new $tableName())->table;
        }
        
        $fields = DbHelper::getFields($tableName, DB::connection()->getDatabaseName());
        $realTable = $tableName;
        if ($asArray) {
            foreach ((array) $fields as $name => $field) {
                $fields[$name] = [
                'table_name' => $realTable,
                'column_name' => $field->getName(),
                'column_key' => '',
                ];
            }
        }
        
        return $fields;
    }
    
    /**
    * @param string $tableName
    *
    * @return array
    */
    public static function getDbIndexes($tableName, $asArray = false)
    {
        if (class_exists($tableName)) {
            $tableName = (new $tableName())->table;
        }
        
        $fields = DbHelper::getIndexes($tableName, DB::connection()->getDatabaseName());
        $realTable = $tableName;
        if ($asArray) {
            foreach ((array) $fields as $name => $field) {
                $fields[$name] = [
                'table_name' => $realTable,
                'column_name' => $field->getName(),
                'column_key' => $field->getName(),
                ];
            }
        }
        
        return $fields;
    }
    
    /**
    * @param string $tableName
    *
    * @return array
    */
    public static function getPrimaryIndex($tableName = '')
    {
        if (class_exists($tableName)) {
            $tableName = (new $tableName())->table;
        }
        $index_keys = static::getDbIndexes($tableName);
        if (isset($index_keys['primary'])) {
            $index = $index_keys['primary'];
        } else {
            $index = array_shift($index_keys);
        }
        
        return $index;
    }
    
    /**
    * @param string $tableName
    *
    * @return array
    */
    public static function getPrimaryKey($tableName = '')
    {
        $index = static::getPrimaryIndex($tableName);
        
        if ($index) {
            return implode(', ', $index->getColumns());
        } else {
            return '';
        }
    }
    
    /**
    * Checks and clears for expired logs.
    *
    * @param int $deleteDaysAfter
    *
    * @return bool
    */
    public static function purgeExpiredLogs($deleteDaysAfter = 31)
    {
        // We're disabling this for now for performacne reasons
        //  if (Schema::hasTable('nitm_api_logs')) {
        //      if (DB::table('nitm_api_logs')->count('created_at')) {
        //          DB::table('nitm_api_logs')
        //          ->whereRaw("cast(created_at AS TIMESTAMP) < NOW() - INTERVAL '".intval($deleteDaysAfter)." days'")
        //          ->delete();
        //      }
        //  }
    }
    
    /**
    * Helper method for event handler which posts data to defined URI with proper way.
    */
    public function handleEvents()
    {
        $allEventHandlers = Eventhandler::all();
        
        foreach ($allEventHandlers as $eventHandler) {
            $model = $eventHandler->model;
            $action = $eventHandler->action;
            
            $model::{$action}(function ($modelObj) use ($eventHandler) {
                $postData = [];
                
                // defaults to sent
                $postData['api_model'] = $eventHandler->model;
                $postData['api_action'] = $eventHandler->action;
                
                $postData = array_merge($postData, $modelObj->getAttributes());
                
                $methods = [
                'curl_post' => 'makeCurlPost',
                'stream_post' => 'makeStreamPost',
                'socket_post' => 'makeSocketPost',
                ];
                $whichPostMethod = $methods[$eventHandler->sendmethod];
                $this->{$whichPostMethod}($eventHandler, $postData);
                
                $time = Carbon::now();
                
                $statusString = substr(
                is_array($this->eventPostResponse) ? $this->eventPostResponse[0] : $this->eventPostResponse,
                0,
                12
                );
                
                if (stripos($statusString, '200') !== false ||
                    stripos($statusString, '302') !== false ||
                stripos($statusString, '100') !== false
                ) {
                    $status = 1;
                } else {
                    $status = 0;
                }
                
                /* Save log to DB */
                $activity = new Eventlog();
                $activity->model = $eventHandler->model;
                $activity->action = $eventHandler->action;
                $activity->url = $eventHandler->url;
                $activity->sendmethod = $eventHandler->sendmethod;
                $activity->model_id = ($modelObj->id ? $modelObj->id : 0);
                $activity->status = $status;
                $activity->response = is_array($this->eventPostResponse) ?
                implode("\n", $this->eventPostResponse) :
                $this->eventPostResponse;
                
                $activity->created_at = $time;
                $activity->updated_at = $time;
                
                return $activity->save();
            });
        }
    }
    
    /**
    * [makeCurlPost description].
    *
    * @param Eventhandler $eventHandler
    * @param  $postData
    */
    public function makeCurlPost(Eventhandler $eventHandler, $postData)
    {
        /*
        // get, post, delete, patch, put, options
        $result = Http::post($eventHandler->url, function($http) use ($postData) {
        $http->header('User-Agent', $this->userAgentString);
        $http->data($postData);
        $http->timeout(30);
        $http->setOption(CURLOPT_VERBOSE, true);
        });

        $this->eventPostResponse = $result->rawBody;
        */
        
        $ch = curl_init($eventHandler->url);
        curl_setopt($ch, CURLOPT_POST, true);
        curl_setopt($ch, CURLOPT_POSTFIELDS, $postData);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_USERAGENT, $this->userAgentString);
        curl_setopt($ch, CURLOPT_VERBOSE, true);
        curl_setopt($ch, CURLOPT_HEADER, true);
        curl_setopt($ch, CURLOPT_CONNECTTIMEOUT, 15);
        curl_setopt($ch, CURLOPT_TIMEOUT, 30);
        
        $exec = curl_exec($ch);
        
        $this->eventPostResponse = $exec;
        curl_close($ch);
    }
    
    /**
    * [makeStreamPost description].
    *
    * @param Eventhandler $eventHandler
    * @param  $postData
    */
    public function makeStreamPost(Eventhandler $eventHandler, $postData)
    {
        $postData = http_build_query($postData);
        
        $contextOptions = [
        'http' => [
        'method' => 'POST',
        'header' => "Content-type: application/x-www-form-urlencoded\r\nContent-Length: ".strlen($postData)."\r\nUser-Agent: {$this->userAgentString}\r\n",
        'content' => $postData,
        ],
        ];
        
        $context = stream_context_create($contextOptions);
        $fp = fopen($eventHandler->url, 'rb', false, $context);
        fclose($fp);
        
        $this->eventPostResponse = $http_response_header;
    }
    
    /**
    * [makeSocketPost description].
    *
    * @param Eventhandler $eventHandler
    * @param  $postData
    */
    public function makeSocketPost(Eventhandler $eventHandler, $postData)
    {
        $del = [
        'http://' => '',
        'https://' => '',
        ];
        
        $urlToPost = strtr($eventHandler->url, $del);
        
        try {
            $fp = fsockopen($urlToPost, 80);
            
            $postData = http_build_query($postData);
            
            fwrite($fp, "POST $urlToPost HTTP/1.1\r\n");
            fwrite($fp, "Host: $urlToPost\r\n");
            fwrite($fp, "Content-Type: application/x-www-form-urlencoded\r\n");
            fwrite($fp, 'Content-Length: '.strlen($postData)."\r\n");
            fwrite($fp, 'User-Agent: '.$this->userAgentString."\r\n");
            fwrite($fp, "Connection: close\r\n");
            fwrite($fp, "\r\n");
            
            fwrite($fp, $postData);
            
            while (!feof($fp)) {
                $this->eventPostResponse .= fgets($fp, 1024);
            }
        } catch (\Exception $e) {
            $this->eventPostResponse = $e->getMessage();
        }
    }
    
    /**
    * Get the parameters for the route.
    *
    * @param string     $req The entity type of request [user, art,...]
    * @param string|int $id  The specific id we need to find
    * @param string     $do  The action to run
    * @param string     $key The primary key for the record we are searching for
    *
    * @return array The route parameters
    */
    public static function getParameters($id = null, $key = 'id')
    {
        $parameters = Route::current()->parameters();
        $id = $id ?: Route::current()->parameter('id');
        if ($id != null) {
            $parameters = array_merge($parameters, [
            'id' => $id,
            'key' => $key,
            ]);
        }
        
        return $parameters;
    }
}
