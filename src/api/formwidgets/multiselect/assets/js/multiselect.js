function updateFields(data, fieldid) {
	var options = '';
	$.each(data, function(index, value) {
		if (value.table_name == window.newTableName || value.class_name == window.newTableName) {
			if (value.hasOwnProperty('column_key') && value.column_key != '') {
				options += "<option value=" + value.column_name + ">" + value.column_name + "</option>";
			}
		}
	});
	options += "</optgroup>";

	options += '<optgroup label="Other Columns">';
	$.each(data, function(index, value) {
		if (value.table_name == window.newTableName || value.class_name == window.newTableName) {
			if (value.hasOwnProperty('column_key') && value.column_key == '' || !value.hasOwnProperty('column_key')) {
				options += "<option value=" + value.column_name + ">" + value.column_name + "</option>";
			}
		}
	});
	options += "</optgroup>";
	$("." + fieldid).html(' ').html(options).select2('close').select2('open');
}
