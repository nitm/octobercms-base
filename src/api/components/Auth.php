<?php

namespace Nitm\Api\Components;

use Lang;
use Request;
use October\Rain\Exception\ApplicationException;
use Nitm\Api\Classes\Trivet;
use Nitm\Api\Models\Configs as RestfulConfig;

class Auth extends Api
{
    protected $auth;

    protected static function getControllerClass()
    {
        return \Nitm\Api\Controllers\AuthController::class;
    }

    public function componentDetails()
    {
        return [
            'name' => 'nitm.api::lang.components.auth.name',
            'description' => 'nitm.api::lang.components.auth.desc',
        ];
    }

    public function defineProperties()
    {
        return [
            'apiStatus' => [
                'title' => 'nitm.api::lang.settings.auth_status.title',
                'description' => 'nitm.api::lang.settings.auth_status.component_detail',
                'type' => 'dropdown',
                'default' => 'on',
                'options' => [
                    'on' => 'nitm.api::lang.settings.auth_status.on',
                    'off' => 'nitm.api::lang.settings.auth_status.off',
                ],
            ],
        ];
    }

    protected function getExtendedFieldsAttribute()
    {
        return [
         'iu_' => [
            'gender',
            'about',
            'company',
            'webpage',
            'job',
         ],
      ];
    }

    /**
     * Starter method of the component.
     *
     * @return string
     */
    public function onRun()
    {
        /*
          * Do some common checks before running the operation
          */
         $methodToRun = $this->beforeRun();
        if ($methodToRun instanceof \Illuminate\Http\JsonResponse) {
            return $methodToRun;
        }

        return $this->getResult([$this, $methodToRun]);
    }

    /**
     * Check if "req" parameter matches with any relation or table, else stop.
     */
    protected function checkIfRequestOk()
    {
        $routeUri = $this->controller->getRouter()->current()->getUri();
        $action = Trivet::getInputs('action') ?: Trivet::getInputs('do') ?: substr($routeUri, strrpos($routeUri, '/') + 1);
        $this->controller->getRouter()->current()->setParameter('action', $action);
        $methods = $this->controller->fields[$action]['methods'];
        if (!in_array(strtolower(Request::method()), $methods)) {
            Trivet::addApiLog(0, 400);
            throw new ApplicationException(sprintf('%s: %s. %s: %s',
             trans('nitm.api::lang.responses.unsupported_method'),
             Request::method(),
             trans('nitm.api::lang.responses.supported_methods'),
             implode(', ', array_map('strtoupper', $methods))
          ));
        }
      /* Check if "req" parameter matches with any relation */
      switch ($action) {
         case 'login':
         if (empty(Trivet::getInputs('login'))) {
             Trivet::addApiLog(0, 400);
             throw new ApplicationException(trans('nitm.api::lang.responses.auth_empty_user'));
         }
         break;
      }
    }

    /**
     * Check if mandatory fields and 'key' field is present.
     *
     * @param $fields
     */
    public function checkIfParamsOk()
    {
        $fields = $this->controller->fields[$this->getOperation()];
        /* Check for mandatory fields (other than key) */
        foreach ($fields['mandatory'] as $value) {
            /* If Authenticate only with User Credentials option activated, pass auth param */
            if (RestfulConfig::get('auth_with_user') && $value == 'auth') {
                continue;
            }

            if (!Trivet::getInputs($value)) {
                Trivet::addApiLog(0, 420);
                throw new ApplicationException(trans('nitm.api::lang.responses.mandatory_fields_mismatch', [
                    'field' => $value,
                ]));
            }
        }
    }

    protected function beforeRun()
    {
        parent::beforeRun();

        /*
         * Check if user is authorized for this operation?
         */
        $this->helper->checkIfAuthCorrect($this->requiresAuth());

         /*
        * Check "do" parameter and determine the Operation which needs to run
        */
        return $this->helper->checkIfAuthCRUD($this->controller->operations);
    }
}
