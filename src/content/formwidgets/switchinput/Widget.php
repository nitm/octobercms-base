<?php

namespace Nitm\Content\FormWidgets\SwitchInput;

use Backend\Classes\FormWidgetBase;
use HTML;
use RainLab\Location\Models\Setting;

class Widget extends FormWidgetBase
{
    public $column;
    public $record;

    /**
     * {@inheritdoc}
     */
    public $defaultAlias = 'nitm-switch';

    /**
     * {@inheritdoc}
     */
    public function render()
    {
        $this->prepareVars();

        return $this->makePartial('widget');
    }

    /**
     * Prepares the list data.
     */
    public function prepareVars()
    {
        $this->vars['name'] = $this->formField->getName();
        $this->vars['value'] = $this->getLoadValue();
        $this->vars['field'] = $this->formField;
        $this->vars['column'] = $this->column;
        $this->vars['record'] = $this->model;
    }

    /**
     * {@inheritdoc}
     */
    public function loadAssets()
    {
    }
}
