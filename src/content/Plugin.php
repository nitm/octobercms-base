<?php namespace Nitm\Content;

use Backend;
use System\Classes\PluginBase;

class Plugin extends PluginBase
{
    /**
     * Returns information about this plugin.
     *
     * @return array
     */
    public function pluginDetails()
    {
        return [
            'name' => 'App',
            'description' => 'Nitm content plugin',
            'author' => 'App',
            'icon' => 'icon-leaf',
        ];
    }

    public function boot()
    {
        static::bootFixes();
        \App::singleton('user.auth', function () {
            $class = \Config::get('app.authManagerClass') ?: \Nitm\Content\Classes\AuthManager::class;
            return $class::instance();
        });

        //Enable SQL query logging
        if (\App::environment() != 'production') {
            trace_sql();
        }
    }

    public static function bootFixes()
    {

        /* Fix for missing oid type in RainlabBuider **/
        $schemaManager = \Schema::getConnection()->getDoctrineSchemaManager();
        \Doctrine\DBAL\Types\Type::addType('oidtype', 'Nitm\Content\Classes\OidDbType');
        // Fixes the problem with oid column type not supported
        // by Doctrine (https://github.com/laravel/framework/issues/1346)
        $platform = $schemaManager->getDatabasePlatform();
        $platform->registerDoctrineTypeMapping('oid', 'oidtype');
        $platform->registerDoctrineTypeMapping('json', 'string');
    }

    public static function extendSideMenu()
    {
        return [
            "pageconfig-menu-item" => [
                "label" => 'Page Config',
                "url" => Backend::url("nitm/content/pageconfig"),
                "icon" => "icon-gear",
                "owner" => "Nitm.Content"
            ],
            "category-menu-item" => [
                "label" => "Categories",
                "url" => Backend::url("nitm/content/category"),
                "icon" => "icon-sitemap",
                "owner" => "Nitm.Content"
            ],
            "features-menu-item" => [
                "label" => "Features",
                "url" => Backend::url("nitm/content/features"),
                "icon" => "icon-sitemap",
                "owner" => "Nitm.Content"
            ],
            "favorite-menu-item" => [
                "label" => "Favorites",
                "url" => Backend::url("nitm/content/favorite"),
                "icon" => "icon-heart",
                "owner" => "Nitm.Content"
            ],
            "follow-menu-item" => [
                "label" => "Follows",
                "url" => Backend::url("nitm/content/follow"),
                "icon" => "icon-users",
                "owner" => "Nitm.Content"
            ],
            "rating-menu-item" => [
                "label" => "Ratings",
                "url" => Backend::url("nitm/content/rating"),
                "icon" => "icon-thumbs-up",
                "owner" => "Nitm.Content"
            ],
        ];
    }

    public static function extendUser()
    {
        $class = '\Rainlab\User\Controllers\Users';
        if (class_exists($class)) {
            $class::extend(function ($controller) {
                //Add a custom nav tool bar with extra buttons
                // $controller->listConfig = plugins_path().'/nitm/content/controllers/users/config_list.yaml';
            });
        }
    }

    public function registerMailTemplates()
    {
        return [
                'nitm.content::mail.validate'   => 'Email to validate University Email.'
        ];
    }

    public function registerComponents()
    {
        return [
            'Nitm\Content\Components\UserActions' => 'userActions',
            'Nitm\Content\Components\Followers' => 'followers',
            'Nitm\Content\Components\Following' => 'following',
            'Nitm\Content\Components\Pagination' => 'pagination',
            'Nitm\Content\Components\MoreButton' => 'moreButton',
            'Nitm\Content\Components\Gallery' => 'gallery',
            'Nitm\Content\Components\Posts' => 'Posts'

        ];
    }

    public function registerFormWidgets()
    {
        return [
           'Nitm\Content\FormWidgets\Address\Widget' => [
               'label' => 'Address',
               'code' => 'owl-address',
           ],
           'Owl\FormWidgets\Money\Widget' => [
               'label' => 'Money',
               'code' => 'owl-money',
           ],
       ];
    }

    public function registerListColumnTypes()
    {
        return [
            'nitm-switch' => function ($value, $column, $record) {
                $controller = \Route::current()->controller;
                $field = new \Backend\Classes\FormField($column->columnName, $column->label);
                $widget = new \Nitm\Content\FormWidgets\SwitchInput\Widget($controller, $field);
                $widget->model = $record;
                $widget->column = $column;
                return $widget->render();
            }
        ];
    }
}
