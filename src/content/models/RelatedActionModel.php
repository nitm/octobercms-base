<?php

namespace Nitm\Content\Models;

use Model;

/**
 * Model.
 */
class RelatedActionModel extends Model
{
    use \Nitm\Content\Traits\Model;
    public $implement = [];
}
