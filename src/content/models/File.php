<?php

namespace Nitm\Content\Models;

use Nitm\Content\Classes\ImageHelper;

class File extends \System\Models\File
{
    /*
     * I need to filter this for returning JSON data.
     *
     * @return [type] [description]
     */
    public function attributesToArray()
    {
        $result = parent::attributesToArray();

        return [
            'id' => $result['id'],
            'url' => $result['path'],
            'path' => $result['path'],
            'title' => $result['title'],
            'lazy' => ImageHelper::createOrGetLazy($this),
            'thumb' => ImageHelper::createOrGetThumbnail($this, 256, 256),
         ];
    }
}
