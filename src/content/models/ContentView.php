<?php

namespace Nitm\Content\Models;

use Request;
use Nitm\Content\Behaviors\Blamable;

class ContentView
{
    protected $_type;
    protected $model;
    protected $_user;

    public function __construct($type = null)
    {
        $type = $type ?? \Config::get('octopus.statistics.model.driver') ?? 'db';
        $this->model = static::createModel($type);
        $this->_type = $type;
        $this->_user = Blamable::getCurrentUser();
    }

    protected function setType($type)
    {
        $this->_type = $type;
    }

    protected function getType()
    {
        return $this->_type;
    }

    public function __get($property)
    {
        if (property_exists($this, $property)) {
            return $this->{$property};
        } else {
            return $this->model->{$property};
        }
    }

    public function __set($property, $value)
    {
        if (property_exists($this, $property)) {
            return $this->{$property} = $value;
        } else {
            return $this->model->{$property} = $value;
        }
    }

    public function __call($method, $parameters)
    {
        if (method_exists($this, $method)) {
            return call_user_func_array([$this, $method], $parameters);
        } else {
            return call_user_func_array([$this->model, $method], $parameters);
        }
    }

    public function save()
    {
        $user = $this->_user;
        $this->model->user = $user ? $user->toArray() : [];
        $this->model->id = md5(\Request::server('REQUEST_URI').'?'.http_build_query($_POST)).'-'.uniqid();
        $this->fillLogInfo();
        $this->model->save();
    }

    /**
     * Get the base log information we want to send.
     *
     * @param [type] $action     [description]
     * @param int    $statusCode [description]
     * @param int    $timePassed [description]
     *
     * @return [type] [description]
     */
    protected function getCoreLogInfo()
    {
        if (Request::server('REQUEST_METHOD') == 'POST') {
            $fullUrl = Request::server('REQUEST_URI').'?'.http_build_query($_POST);
        } else {
            $fullUrl = Request::server('REQUEST_URI');
        }

        $time = date('Y-m-d H:i:s');
        $token = 'none';
        if ($this->_user) {
            $token = $this->_user->apiToken ? $this->_user->apiToken->token : 'none';
        }

        return [
            'ip' => array_get($_SERVER, 'REMOTE_ADDR'),
            'ip_spoofed' => array_get($_SERVER, 'HTTP_CLIENT_IP', array_get($_SERVER, 'HTTP_X_FORWARDED_FOR'), array_get($_SERVER, 'REMOTE_ADDR')),
            'ip_forwarded' => array_get($_SERVER, 'HTTP_X_FORWARDED_FOR'),
            'used_key' => json_encode($token),
            'referer' => Request::server('HTTP_REFERER'),
            'browser' => Request::server('HTTP_USER_AGENT'),
            'cookie_hash' => md5(json_encode($_COOKIE)),
            'url' => $fullUrl,
            'request_method' => Request::server('REQUEST_METHOD'),
            'created_at' => $time,
            'host' => array_get($_SERVER, 'REMOTE_ADDR'),
      ];
    }

    protected function fillLogInfo()
    {
        $info = $this->getCoreLogInfo();
        if (class_exists('BrowserDetect')) {
            ini_set('memory_limit', '2G');
            $browserInfo = \BrowserDetect::detect()->toArray();
            $info = array_merge($info, $browserInfo);
        }
        $this->model->fill(array_combine(array_map('snake_case', array_keys($info)), $info));
    }

    public static function createModel($type)
    {
        $modelName = studly_case($type.'ContentView');
        $class = '\Nitm\Content\Models\ContentView\\'.$modelName;
        if (class_exists($class)) {
            $model = new $class();

            return $model;
        } else {
            throw new \Exception("Cannot find $class");
        }
    }
}
