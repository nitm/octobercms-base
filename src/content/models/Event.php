<?php

namespace Nitm\Content\Models;

use Model;
use DB;
use Validator;
use Carbon\Carbon;

/**
 * Model.
 */
class Event extends BaseContent
{
    use \Nitm\Content\Traits\Feature;

    const STATUS_CANCELED = 'canceled';
    const STATUS_POSTPONED = 'postponed';
    const STATUS_NORMAL = 'normal';

    /*
     * Validation
     */
    public $rules = [
        'starts_at' => 'required|date',
        // 'ends_at' => 'date|after:starts_at',
        'description' => 'required',
      //   'category_id' => 'required|integer',
      //   'type_id' => 'required|integer',
        'title' => 'required|unique_event:app_content_events',
        // 'image' => 'required',
    ];

    public $customMessages = [
      'unique_event' => 'You already created this event. Check your events and update the existing one!',
      'category_id' => 'Please select an event category',
      'type_id' => 'Please select an event type',
    ];

    protected $slugs = [
      'slug' => ['title', 'typeSlug', 'categorySlug'],
   ];

    public $fillable = [
      'is_free', 'title', 'author_id',
      'created_at', 'updated_at', 'starts_at', 'ends_at',
      'postponed', 'status', 'cost',
      'type_id', 'category_id','type',
      'category', 'featureType',
      'description', 'image', 'logo', 'location',
   ];

    public $visible = [
      'id', 'is_free', 'title', 'author', 'slug', 'starts_at', 'ends_at',
      'start', 'end', 'postponed_to', 'status',
      'cost', 'type', 'category', 'is_featured',
      'description', 'location', 'image', 'logo', 'date',
   ];

    public $with = [
      'image', 'logo', 'type', 'author', 'category', 'locations', 'featureType', 'features'
   ];

    public $eagerWith = [
      'locations', 'attendees', 'attendeesGoing', 'attendeesMaybeGoing', 'attendeesNotGoing', 'attendeesCount', 'attendeesGoingCount', 'attendeesMaybeGoingCount', 'attendeesNotGoingCount',
   ];

    /*
     * Disable timestamps by default.
     * Remove this line if timestamps are defined in the database table.
     */
    public $timestamps = true;

    public $dates = [
        'starts_at', 'ends_at'
    ];

    /**
     * @var string The database table used by the model
     */
    public $table = 'app_content_events';

    public $belongsTo = [
      'type' => ['App\Content\Models\EventType', 'key' => 'type_id', 'otherKey' => 'id'],
      'category' => ['App\Content\Models\EventCategory', 'key' => 'category_id', 'otherKey' => 'id'],
      'featureType' => ['App\Content\Models\FeatureType', 'key' => 'feature_type_id', 'otherKey' => 'id'],
   ];

    public $appends = [
        'start', 'end', 'date', 'shortDate',
        'location', 'is_featured',
        'attendeesCount', 'attendeesGoingCount', 'attendeesNotGoingCount', 'attendeesMaybeGoingCount'
    ];

    public $hasMany = [
       'attendees' => [
         'Nitm\Content\Models\RelatedEventAttendee',
         'otherKey' => 'id',
         'key' => 'event_id',
       ],
       'attendeesGoing' => [
         'Nitm\Content\Models\RelatedEventAttendee',
         'otherKey' => 'id',
         'key' => 'event_id',
         'condition' => "status = '".EventAttendee::STATUS_GOING."'",
       ],
       'attendeesMaybeGoing' => [
         'Nitm\Content\Models\RelatedEventAttendee',
         'otherKey' => 'id',
         'key' => 'event_id',
         'condition' => "status = '".EventAttendee::STATUS_MAYBE."'",
       ],
       'attendeesNotGoing' => [
         'Nitm\Content\Models\RelatedEventAttendee',
         'otherKey' => 'id',
         'key' => 'event_id',
         'condition' => "status = '".EventAttendee::STATUS_NOT_GOING."'",
       ],
   ];

    public $hasOne = [
      'locations' => [
         'Nitm\Content\Models\LocationList',
         'key' => 'item_id',
         'otherKey' => 'id',
         'conditions' => "item_type='event'",
      ],
       'attendeesCount' => [
         'Nitm\Content\Models\RelatedEventAttendee',
         'otherKey' => 'id',
         'key' => 'event_id',
         'count' => true,
       ],
       'attendeesGoingCount' => [
         'Nitm\Content\Models\RelatedEventAttendee',
         'otherKey' => 'id',
         'key' => 'event_id',
         'condition' => "status = '".EventAttendee::STATUS_GOING."'",
         'count' => true,
       ],
       'attendeesMaybeGoingCount' => [
         'Nitm\Content\Models\RelatedEventAttendee',
         'otherKey' => 'id',
         'key' => 'event_id',
         'condition' => "status = '".EventAttendee::STATUS_MAYBE."'",
         'count' => true,
       ],
       'attendeesNotGoingCount' => [
         'Nitm\Content\Models\RelatedEventAttendee',
         'otherKey' => 'id',
         'key' => 'event_id',
         'condition' => "status = '".EventAttendee::STATUS_NOT_GOING."'",
         'count' => true,
       ],
    ];

    public $attachOneDefault = [
      'image' => 'Nitm\Content\Models\File',
      'logo' => 'Nitm\Content\Models\File',
    ];

    protected $_location;
    protected $_sessionKey;

    protected static function setupCustomValidators()
    {
        Validator::extend('unique_event', function ($attribute, $value, $parameters, $validator) {
            // Get table name from first parameter
            $table = array_shift($parameters);

            // Build the query
            $query = DB::table($table);
            $query->select('id');
            // Add the field conditions
            $data = $validator->getData();
            $query->where([
               'title' => array_get($data, 'title'),
               'starts_at' => array_get($data, 'starts_at'),
               'ends_at' => array_get($data, 'ends_at'),
               'type_id' => explode(',', array_get($data, 'type_id', 0))[0],
               'category_id' => explode(',', array_get($data, 'category_id', 0))[0],
            ]);

            // Validation result will be false if any rows match the combination
            return $query->count() == 0;
        });
    }

    public function beforeSave()
    {
        $this->attributes['starts_at'] = $this->starts_at ?: Carbon::now();
        $this->attributes['status'] = array_get($this->attributes, 'status', static::STATUS_NORMAL);
        $this->dedupe();
        $this->_sessionKey = uniqid('session_key', true);
        unset($this->attributes['setLocation']);
    }

    public function beforeValidate()
    {
        $this->slugAttributes();
    }

    /**
     * Get the supported art types.
     *
     * @param bool $getQuery Should we requrn the query?
     *
     * @return \Illuminate\Eloquent\(Collection|Builder) result
     */
    public function supportedTypes($getQuery = false)
    {
        return $this->getWithChildren(EventType::class, $getQuery);
    }

    /**
     * Get the supported art types.
     *
     * @param bool $getQuery Should we requrn the query?
     *
     * @return \Illuminate\Eloquent\(Collection|Builder) result
     */
    public function supportedCategories($getQuery = false)
    {
        return $this->getWithChildren(EventCategory::class, $getQuery);
    }

    /**
     * Get the art type options.
     *
     * @return array The type ID options
     */
    public function getTypeIdOptions()
    {
        return $this->getNestedDropdownOptions('supportedTypes');
    }

    /**
     * Get the art type options.
     *
     * @return array The type ID options
     */
    public function getCategoryIdOptions()
    {
        return $this->getNestedDropdownOptions('supportedCategories');
    }

    /**
     * Get the art type options.
     *
     * @return array The type ID options
     */
    public static function getStatusOptions()
    {
        return [
           self::STATUS_CANCELED => 'Canceled',
           self::STATUS_POSTPONED => 'Postponed',
           self::STATUS_NORMAL => 'Normal',
        ];
    }

    // public function relationsToArray()
    // {
    //     //Get relations and exclude sensitive information
    //     $attributes = array_intersect_key(parent::relationsToArray(), array_flip(array_map('snake_case', $this->with)));
    //     unset($attributes['locations']);

    //     return $attributes;
    // }

    public function attributesToArray()
    {
        //Get relations and exclude sensitive information
        $attributes = parent::attributesToArray();

        return $attributes;
    }

    // Scopes
    public function scopeFilterByUpcoming($query)
    {
        return $query
            ->where('starts_at', '>', Carbon::now())
        ;
    }

    public function scopeFilterByPast($query)
    {
        return $query
            ->where('starts_at', '<', Carbon::now())
        ;
    }

    public function scopeFilterByFeatured($query)
    {
        return $query
            ->whereIn('id', function ($query) {
                $query->from((new FeatureLink)->getTable())
                  ->select(['id'])
                  ->where([
                     'remote_id' => $this->id,
                     'remote_type' => 'event-feature-type'
                  ])
                  ->whereIn('feature_id', function ($query) {
                      $query->from((new Feature)->getTable())
                        ->select(['id'])
                        ->where([
                           'is_active' => true
                        ]);
                  });
            })
        ;
    }

    /**
     * Filter art by the types.
     *
     * @param [type] $query     [description]
     * @param [type] $types     [description]
     * @param bool   $inclusive Should the query use OR or AND?
     *
     * @return [type] [description]
     */
    public function scopeFilterByType($query, $types, $inclusive = false)
    {
        return $this->scopeFilterByCategory($query, $types, $inclusive, [
           'categoryClass' => EventType::class,
           'field' => 'type_id'
        ]);
    }

    /**
     * Filter art by the category.
     *
     * @param [type] $query     [description]
     * @param [type] $types     [description]
     * @param bool   $inclusive Should the query use OR or AND?
     *
     * @return [type] [description]
     */
    public function scopeFilterByTypes($query, $types, $inclusive = false)
    {
        return $this->scopeFilterByType($query, $types, $inclusive);
    }

    /**
     * Filter art by the category.
     *
     * @param [type] $query     [description]
     * @param [type] $types     [description]
     * @param bool   $inclusive Should the query use OR or AND?
     *
     * @return [type] [description]
     */
    public function scopeFilterByCategories($query, $categories, $inclusive = false)
    {
        return $this->scopeFilterByCategory($query, $categories, $inclusive);
    }

    /**
     * Filter art by the category.
     *
     * @param [type] $query     [description]
     * @param [type] $types     [description]
     * @param bool   $inclusive Should the query use OR or AND?
     *
     * @return [type] [description]
     */
    public function scopeFilterByCategory($query, $categories, $inclusive = false, $options=[])
    {
        extract($options);
        $categoryClass = isset($categoryClass) ? $categoryClass : EventCategory::class;
        $field = isset($field) ? $field : 'category_id';
        $method = $inclusive ? 'orWhere' : 'where';
        $query->$method(function ($query) use ($categories, $categoryClass, $field) {
            $categories = (array) $categories;
            $categoryQuery = $categoryClass::getExistingFrom($categories);
            $ids = $categoryQuery->lists('id');
            $query->whereIn($field, $ids);
        });
    }

    // Attribute prepareers, getters and setters

    public function prepareCategoryAttribute($categories)
    {
        $existing = EventCategory::getSingleExistingFrom($categories, 'title');

        return $existing;
    }

    public function prepareTypeAttribute($categories)
    {
        $existing = EventType::getSingleExistingFrom($categories, 'title');

        return $existing;
    }

    public function prepareFeatureTypeAttribute($categories)
    {
        $existing = FeatureType::getSingleExistingFrom($categories, 'title');

        return $existing;
    }

    public function setLocationAttribute($location)
    {
        if (!empty(array_filter($location))) {
            $model = $this->locations ?: new LocationList([
                'item_type' => 'event',
            ]);
            $model->setLocationAttribute($location);
            $this->saveRelation('locations', $model, 'save');
        }
    }

    public function getIsFeaturedAttribute()
    {
        if ($this->feature) {
            return $this->feature->is_active;
        }
        return false;
    }

    public function getLocationAttribute()
    {
        return $this->locations ? $this->locations->location : new Location();
    }

    public function getTypeTitleAttribute()
    {
        return $this->type->title;
    }

    public function getCategoryTitleAttribute()
    {
        return $this->category->title;
    }

    // public function prepareStartsAtAttribute($date)
    // {
    //     $date = is_numeric($date) ? $date : strtotime($date ?: 'now');
    //     return date('Y-m-d H:i:s', $date);
    // }

    // public function prepareEndsAtAttribute($date)
    // {
    //     return $date ? date('Y-m-d H:i:s', strtotime($date)) : null;
    // }

    public function setCostAttribute($value)
    {
        $this->attributes['cost'] = floatval($value ?: 0);
    }

    protected function getAttendeesCountAttribute()
    {
        return $this->getRelationCount('attendees');
    }

    protected function getAttendeesGoingCountAttribute()
    {
        return $this->getRelationCount('attendeesGoing');
    }

    protected function getAttendeesNotGoingCountAttribute()
    {
        return $this->getRelationCount('attendeesNotGoing');
    }

    protected function getAttendeesMaybeGoingCountAttribute()
    {
        return $this->getRelationCount('attendeesMaybeGoing');
    }

    protected function getDateAttribute()
    {
        return [
           'day' => $this->startDay,
           'month' => $this->startMonth,
           'year' => $this->startYear,
           'time' => $this->startTime,
           'value' => $this->startMonth.' '.$this->startDay.', '.$this->startYear,
        ];
    }

    protected function getShortDateAttribute()
    {
        $date = $this->startMonth.' '.$this->startDay;
        if ($this->startYear != $this->endYear) {
            $date .= ', '.$this->endYear;
        }
        if ($this->endMonth && $this->endMonth == $this->startMonth && $this->startDay != $this->endDay) {
            $date .= '-'.$this->endDay;
        } elseif ($this->endMonth && $this->startDay != $this->endDay) {
            $date .= '-'.$this->endMonth.' '.$this->endDay;
        }
        if ($this->endYear && $this->endYear != $this->startYear) {
            $date .= ', '.$this->endYear;
        }

        return $date;
    }

    public function getStartAttribute()
    {
        return [
           'day' => $this->startDay,
           'month' => $this->startMonth,
           'year' => $this->startYear,
           'time' => $this->startTime,
           'value' => $this->startMonth.' '.$this->startDay.', '.$this->startYear,
        ];
    }

    public function getStartTimeAttribute()
    {
        return Carbon::parse($this->starts_at)->format('g:i A');
    }

    public function getStartDayAttribute()
    {
        return Carbon::parse($this->starts_at)->format('D');
    }

    public function getStartMonthAttribute()
    {
        return Carbon::parse($this->starts_at)->format('M');
    }

    public function getStartYearAttribute()
    {
        return Carbon::parse($this->starts_at)->format('y');
    }

    public function getEndAttribute()
    {
        return [
           'day' => $this->endDay,
           'month' => $this->endMonth,
           'year' => $this->endYear,
           'time' => $this->endTime,
           'value' => $this->endMonth.' '.$this->endDay.', '.$this->endYear,
        ];
    }

    public function getEndTimeAttribute()
    {
        return Carbon::parse($this->ends_at)->format('g:i A');
    }

    public function getEndDayAttribute()
    {
        return Carbon::parse($this->ends_at)->format('D');
    }

    public function getEndMonthAttribute()
    {
        return Carbon::parse($this->ends_at)->format('M');
    }

    public function getEndYearAttribute()
    {
        return Carbon::parse($this->ends_at)->format('y');
    }

    public function getPostponedAttribute()
    {
        return [
           'day' => $this->postponedDay,
           'month' => $this->postponedMonth,
           'year' => $this->postponedYear,
           'time' => $this->postponedTime,
           'value' => $this->postponedMonth.' '.$this->postponedDay.', '.$this->postponedYear,
        ];
    }

    public function getPostponedTimeAttribute()
    {
        return Carbon::parse($this->postponed_to)->format('g:i A');
    }

    public function getPostponedDayAttribute()
    {
        return Carbon::parse($this->postponed_to)->day;
    }

    public function getPostponedMonthAttribute()
    {
        return Carbon::parse($this->postponed_to)->format('M');
    }

    public function getPostponedYearAttribute()
    {
        return Carbon::parse($this->postponed_to)->format('y');
    }
}
