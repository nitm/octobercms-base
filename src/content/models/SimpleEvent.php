<?php

namespace Nitm\Content\Models;

use Model;

/**
 * The related model minimizes the amount of data eager loaded and the amount of imformation that is implemented by extension.
 * This results in a lighter model for attached to related user actions and the user activity feed.
 */
class SimpleEvent extends RelatedEvent
{
    public $implement = [];

   /*
    * Relations
    */
   public $belongsTo = [
      'author' => ['Nitm\Content\Models\SimpleUser', 'key' => 'author_id'],
      'user' => ['Nitm\Content\Models\SimpleUser', 'key' => 'author_id'],
      'type' => ['Nitm\Content\Models\EventType', 'key' => 'type_id', 'otherKey' => 'id'],
      'category' => ['Nitm\Content\Models\EventCategory', 'key' => 'category_id', 'otherKey' => 'id'],
   ];

    public $with = [
      'image', 'type', 'author', 'category',
   ];

  /**
   * To prevent extending the model beyond the basics.
   * Local extendable construct skips initing parent extendables.
   *
   * @method __construct
   */
  public function __construct($attributes = [])
  {
      $this->bootDefaultRelations();
      $this->bootNicerEvents();
      $this->localExtendableConstruct();
      $this->fill($attributes);
  }

    public function getMorphClass()
    {
        return 'Nitm\Content\Models\Event';
    }
}
