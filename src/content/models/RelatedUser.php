<?php

namespace Nitm\Content\Models;

use Model;

/**
 * Model.
 */
class RelatedUser extends User
{
    public $with = ['avatar'];
    public $eagerWith = [];

    public $implement = [
      'Nitm.Content.Behaviors.Follow',
      'Nitm.Content.Behaviors.Search',
   ];

    public $visible = [
      'id', 'username', 'email', 'name', 'avatar',
   ];

    public $appends = ['fullName'];

    public $hasOne = [];
    public $hasMany = [];

   /**
    * To prevent extending the model beyond the basics.
    * Local extendable construct skips initing parent extendables.
    *
    * @method __construct
    */
   public function __construct($attributes = [])
   {
       $this->bootDefaultRelations();
       $this->bootNicerEvents();
       $this->localExtendableConstruct();
       $this->fill($attributes);
   }

    public function getPublicIdAttribute()
    {
        return $this->username;
    }

    public function attributesToArray()
    {
        $result = parent::attributesToArray();
        unset($result['api_token']);

        return $result;
    }
}
