<?php

namespace Nitm\Content\Models;

use Model;

/**
 * The related model minimizes the amount of data eager loaded and the amount of imformation that is implemented by extension.
 * This results in a lighter model for attached to related user actions and the user activity feed.
 */
class RelatedPost extends Post
{
    public $implement = [
      'Nitm.Content.Behaviors.Search',
      'Nitm.Content.Behaviors.Favorite',
   ];

    public $with = ['author', 'featured_images'];
    public $appends = ['date'];
    public $eagerWith = [];

   /*
    * Relations
    */
   public $belongsTo = [
      'author' => ['Nitm\Content\Models\SimpleUser', 'key' => 'user_id'],
      'user' => ['Nitm\Content\Models\SimpleUser'],
   ];

    public $visible = [
      'id', 'title', 'slug', 'excerpt', 'author', 'categories', 'image',
   ];

  /**
   * To prevent extending the model beyond the basics.
   * Local extendable construct skips initing parent extendables.
   *
   * @method __construct
   */
  public function __construct($attributes = [])
  {
      $this->bootDefaultRelations();
      $this->bootNicerEvents();
      $this->localExtendableConstruct();
      $this->fill($attributes);
  }

    public function getMorphClass()
    {
        return 'Nitm\Content\Models\Post';
    }
}
