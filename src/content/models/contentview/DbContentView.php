<?php

namespace Nitm\Content\Models\ContentView;

use Model;

class DbContentView extends Model
{
    use \Nitm\Content\Traits\Model;
    
    public $table = 'nitm_content_statistics_views';
}