<?php

namespace Nitm\Content\Models;

use Model;

/**
 * Model.
 */
class RelatedRating extends Rating
{
    public $visible = [
     'id', 'value', 'created_at', 'count', 'thing_type', 'thing_id',
   ];
    public $implements = [];
    public $with = [];
    public $appends = [];

    /**
     * Disable construction of parent to prevent __construct loop.
     *
     * @method __construct
     */
    public function __construct()
    {
        $this->bootDefaultRelations();
    }

    public function getMorphClass()
    {
        return 'Nitm\Content\Models\Rating';
    }
}
