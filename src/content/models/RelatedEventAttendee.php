<?php

namespace Nitm\Content\Models;

use Model;

/**
 * Model.
 */
class RelatedEventAttendee extends EventAttendee
{
    public $implement = [];

    public function getMorphClass()
    {
        return 'Nitm\Content\Models\EventAttendee';
    }
}
