<?php

namespace Nitm\Content\Models;

use Model;
use Nitm\Content\Classes\ImageHelper;
use Illuminate\Support\Str;

class BaseContent extends Model
{
    use \Nitm\Content\Traits\Model,
         \October\Rain\Database\Traits\Sluggable,
         \October\Rain\Database\Traits\Validation,
         \October\Rain\Database\Traits\SoftDelete;

   /*
    * Disable timestamps by default.
    * Remove this line if timestamps are defined in the database table.
    */
    public $timestamps = true;

    protected $slugs = [];

    public $blamable = [
      'create' => 'author_id',
   ];

    protected $rules = [];

    public $eagerWith = [];
    public $visible = ['id'];

    public $implement = [
      'Nitm.Content.Behaviors.Blamable',
      'Nitm.Content.Behaviors.Search',
      'Nitm.Content.Behaviors.Permissions',
      'Nitm.Content.Behaviors.Activity',
      'Nitm.Content.Behaviors.Rating',
      'Nitm.Content.Behaviors.Favorite',
   ];

    public $attachOneDefault = [
      'image' => 'Nitm\Content\Models\File',
    ];

    public $attachManyDefault = [
      'images' => 'Nitm\Content\Models\File',
    ];

    public $belongsToDefault = [
      'author' => ['Nitm\Content\Models\RelatedActionUser', 'key' => 'author_id', 'otherKey' => 'id', 'scope' => 'limitRelations'],
      'editor' => ['Nitm\Content\Models\RelatedUser', 'key' => 'editor_id', 'otherKey' => 'id'],
    ];

    public function __construct(array $attributes = [])
    {
        $this->bootDefaultRelations();
        parent::__construct($attributes);
        static::setupCustomValidators();

        if (empty($this->eagerWith)) {
            $this->eagerWith = $this->with;
        }
    }

    protected static function setupCustomValidators()
    {
    }

    public function beforeCreate()
    {
    }

    public function beforeUpdate()
    {
        if (count($this->slugs)) {
            $this->slug = $this->attributes['slug'] = null;
            $this->slugAttributes();
        }
    }

    public function getThumbnailAttribute()
    {
        return ImageHelper::createOrGetThumbnail($this->image, 64, 64);
    }

    public function getMediumThumbnailAttribute()
    {
        return ImageHelper::createOrGetThumbnail($this->image, 128, 128);
    }

    public function getLargeThumbnailAttribute()
    {
        return ImageHelper::createOrGetThumbnail($this->image, 256, 256);
    }

    public function getExtraLargeThumbnailAttribute()
    {
        return ImageHelper::createOrGetThumbnail($this->image, 512, 512);
    }

    public function title()
    {
        return $this->title;
    }

    public function scopeFilterByType($query, $types)
    {
        return false;
    }

    public function getAllImagesCountAttribute()
    {
        return $this->allImages->count();
    }

    public function getAllImagesAttribute()
    {
        $images = $this->images ?: [];
        if ($this->image) {
            $images[$this->image->id] = $this->image;
        }

        return $images;
    }

    public function getMainImageAttribute()
    {
        return $this->image ?: $this->images->first();
    }
}
