<?php

namespace Nitm\Content\Models;

use Model;

/**
 * This model has limited constructon to prevent hte loading of extensions and extra relations
 * This model should be used for the related content for user actions.
 */
class RelatedActionUser extends User
{
    public $eagerWith = [];
    public $with = ['avatar'];
    public $implement = [];
    public $appends = [];

    /**
     * To prevent extending the model beyond the basics.
     *
     * @method __construct
     */
    public function __construct()
    {
        $this->bootDefaultRelations();
    }

    public function attributesToArray()
    {
        $result = parent::attributesToArray();
        unset($result['api_token']);

        return $result;
    }
}
