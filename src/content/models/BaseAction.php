<?php

namespace Nitm\Content\Models;

use Model;

/**
 * Model.
 */
class BaseAction extends Model
{
    use \October\Rain\Database\Traits\Validation,
         \October\Rain\Database\Traits\SoftDelete,
         \Nitm\Content\Traits\Model;

    protected $shouldCache = false;
    protected $isPrepared;

    public $implement = [
      'Nitm.Content.Behaviors.Blamable',
      'Nitm.Content.Behaviors.Search',
      'Nitm.Content.Behaviors.Permissions',
      'Nitm.Content.Behaviors.DynamicContentRelation',
   ];

    public function __construct(array $attributes = [])
    {
        parent::__construct($attributes);
        static::setupCustomValidators();
    }

    public function toArray()
    {
        $attributes = parent::toArray();

        return array_filter($attributes);
    }
}
