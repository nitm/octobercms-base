<?php

namespace Nitm\Content\Controllers;

use Backend\Classes\Controller;
use BackendMenu;

class Event extends Controller
{
    public $implement = [
      'Backend\Behaviors\ListController',
      'Backend\Behaviors\FormController',
      'Backend\Behaviors\ReorderController',
      'Backend.Behaviors.RelationController',
   ];

    public $listConfig = 'config_list.yaml';
    public $formConfig = 'config_form.yaml';
    public $reorderConfig = 'config_reorder.yaml';
    public $relationConfig = 'config_relation.yaml';

    public function __construct()
    {
        parent::__construct();
        BackendMenu::setContext('App.Content', 'content', 'side-menu-item5');
    }

    public function listExtendQuery($query)
    {
        $query->with(['author', 'image', 'type']);
    }

    public function onToggleFree()
    {
        $event = \App\Content\Models\Event::find(\Request::input('id'));
        if ($event) {
            $event->is_free = !$event->is_free;
            $this->vars['record'] = $event;
            $event->save();
            \Flash::success("Successfilly toggled event to [".($event->is_free ? 'free' : 'not free')."]");
            return [
                'value' => $event->is_free
            ];
        } else {
            throw new \ApplicationException('Invalid event!');
        }
    }

    public function onToggleFeatured()
    {
        $event = \App\Content\Models\Event::find(\Request::input('id'));
        if ($event) {
            $isActive = $event->toggleFeature();
            if ($isActive) {
                \Flash::success("Successfilly featured event");
            } else {
                \Flash::success("Successfilly removed feature for event");
            }
            $this->vars['record'] = $event;
            return [
                'value' => $event->feature instanceof Feature
            ];
        } else {
            throw new \ApplicationException('Invalid event!');
        }
    }
}
