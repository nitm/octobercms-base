<?php

namespace Nitm\Content\Controllers;

use Backend\Classes\Controller;
use BackendMenu;

class Features extends Controller
{
    public $implement = [
        'Backend\Behaviors\ListController',
        'Backend\Behaviors\FormController',
        'Backend\Behaviors\ReorderController',
        'Backend.Behaviors.RelationController'
    ];

    public $listConfig = 'config_list.yaml';
    public $formConfig = 'config_form.yaml';
    public $reorderConfig = 'config_reorder.yaml';
    public $relationConfig = 'config_relation.yaml';

    public function __construct()
    {
        parent::__construct();
        BackendMenu::setContext('App.Content', 'content', 'features-menu-item');
    }

    public function listExtendQuery($query)
    {
        $query->with([
           'author', 'link', 'type',
           'link.event.type', 'link.event.author',
           'link.art.type', 'link.art.author', 'link.art.image',
           'link.post.author',
           'link.user.avatar',
        ]);
    }

    public function onToggleActive()
    {
        $feature = \App\Content\Models\Feature::find(\Request::input('id'));
        if ($feature) {
            $feature->is_active = !$feature->is_active;
            $this->vars['record'] = $feature;
            $feature->save();
            \Flash::success("Successfilly toggled feature to [".($feature->is_active ? 'active' : 'inactive')."]");
            return [
                'value' => $feature->is_active
            ];
        } else {
            throw new \ApplicationException('Invalid feature!');
        }
    }
}
