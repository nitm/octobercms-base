<?php

namespace Nitm\Content\Controllers;

use BackendMenu;
use Backend\Classes\Controller;

class Rsvp extends Controller
{
    public $implement = [
      'Backend\Behaviors\ListController',
      'Backend\Behaviors\ReorderController',
   ];

    public $listConfig = 'config_list.yaml';
    public $reorderConfig = 'config_reorder.yaml';

    public function __construct()
    {
        parent::__construct();
        BackendMenu::setContext('App.Content', 'content', 'side-menu-item6');
    }

    public function index_onRefresh()
    {
        return $this->listRefresh();
    }
}
