<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class UpdateLocationListTitle extends Migration
{
    public function up()
    {
        Schema::table('nitm_content_location', function ($table) {
            $table->string('title', 255)->change();
        });
    }

    public function down()
    {
    }
}
