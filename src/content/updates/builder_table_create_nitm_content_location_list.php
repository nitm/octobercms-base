<?php

namespace Nitm\Content\Updates;

use Schema;
use October\Rain\Database\Updates\Migration;

class BuilderTableCreateNitmContentLocationList extends Migration
{
    public function up()
    {
        if (!Schema::hasTable('nitm_content_location_list')) {
            Schema::create('nitm_content_location_list', function ($table) {
                $table->engine = 'InnoDB';
                $table->increments('id');
                $table->integer('location_id');
                $table->integer('item_id');
            });
        }
    }

    public function down()
    {
    }
}
