<?php

namespace Nitm\Content\Updates;

use Schema;
use October\Rain\Database\Updates\Migration;

class BuilderTableCreateNitmContentActivity extends Migration
{
    public function up()
    {
        if (!Schema::hasTable('nitm_content_activity')) {
            Schema::create('nitm_content_activity', function ($table) {
                $table->engine = 'InnoDB';
                $table->increments('id');
                $table->string('title', 128);
                $table->string('verb', 32);
                $table->timestamp('created_at')->nullable();
                $table->text('actor')->nullable();
                $table->text('object')->nullable();
                $table->text('target')->nullable();
                $table->boolean('is_admin_action')->nullable()->default(0);
                $table->timestamp('updated_at')->nullable();
            });
        }
    }

    public function down()
    {
          Schema::dropIfExists('nitm_content_activity');
    }
}
