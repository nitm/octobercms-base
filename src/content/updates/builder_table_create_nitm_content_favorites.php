<?php

namespace Nitm\Content\Updates;

use Schema;
use October\Rain\Database\Updates\Migration;

class BuilderTableCreateNitmContentFavorites extends Migration
{
    public function up()
    {
        if (!Schema::hasTable('nitm_content_favorites')) {
            Schema::create('nitm_content_favorites', function ($table) {
                $table->engine = 'InnoDB';
                $table->integer('id');
                $table->integer('thing_id');
                $table->string('thing_class', 255);
                $table->string('thing_type', 255);
                $table->integer('user_id')->nullable();
                $table->timestamp('deleted_at')->nullable();
                $table->primary(['id']);
            });
        }
    }

    public function down()
    {
        //   Schema::dropIfExists('nitm_content_favorites');
    }
}
