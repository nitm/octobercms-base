<?php

namespace Nitm\Content\Updates;

use Schema;
use October\Rain\Database\Updates\Migration;

class BuilderTableCreateNitmContentLocations extends Migration
{
    public function up()
    {
        if (!Schema::hasTable('nitm_content_location')) {
            Schema::create('nitm_content_location', function ($table) {
                $table->engine = 'InnoDB';
                $table->increments('id');
                $table->string('title', 32);
                $table->text('description')->nullable();
                $table->string('name', 128)->nullable();
                $table->string('city')->nullable();
                $table->string('zip')->nullable();
                $table->string('country')->nullable();
                $table->decimal('latitude', 10, 7)->nullable();
                $table->decimal('longitude', 10, 7)->nullable();
                $table->integer('type_id')->nullable();
                $table->string('address')->nullable();
            });
        }
    }

    public function down()
    {
        //   Schema::dropIfExists('nitm_content_location');
    }
}
