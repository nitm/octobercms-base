<?php

namespace Nitm\Content\Updates;

use Schema;
use October\Rain\Database\Updates\Migration;
use Nitm\Content\Models\FeatureLink;

class BuilderTableUpdateFeatureLinks extends Migration
{
    public function up()
    {
        foreach ([
            'App\Content\Models\Event' => 'event-feature-type',
            'App\Content\Models\Post' => 'post-feature-type',
            'App\Content\Models\User' => 'user-feature-type'
        ] as $class => $slug) {
            FeatureLink::where('remote_type', $slug)
            ->update(['remote_type' => $class]);
        }
    }

    public function down()
    {
    }
}
