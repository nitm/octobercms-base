<?php

namespace Nitm\Content\Updates;

use Schema;
use October\Rain\Database\Updates\Migration;

class BuilderTableUpdateNitmContentLocationList extends Migration
{
    public function up()
    {
        Schema::table('nitm_content_location_list', function ($table) {
            if (!Schema::hasColumn('nitm_content_location_list', 'item_type')) {
                $table->string('item_type', 64)->nullable();
            }
        });
    }

    public function down()
    {
        Schema::table('nitm_content_location_list', function ($table) {
            $table->dropColumn('item_type');
        });
    }
}
