<?php

namespace Nitm\Content\Updates;

use Schema;
use October\Rain\Database\Updates\Migration;

class BuilderTableCreateNitmContentFollows extends Migration
{
    public function up()
    {
        if (!Schema::hasTable('nitm_content_follows')) {
            Schema::create('nitm_content_follows', function ($table) {
                $table->engine = 'InnoDB';
                $table->increments('id');
                $table->string('title', 128)->nullable();
                $table->string('type', 32)->default('follow');
                $table->text('followee');
                $table->text('follower');
                $table->integer('follower_id')->nullable();
                $table->integer('followee_id')->nullable();
                $table->dateTime('start_date');
                $table->dateTime('end_date')->nullable();
                $table->boolean('is_admin_action')->nullable();
                $table->softDeletes();
            });
        }
    }

    public function down()
    {
        //   Schema::dropIfExists('nitm_content_follows');
    }
}
