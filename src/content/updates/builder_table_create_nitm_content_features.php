<?php

namespace Nitm\Content\Updates;

use Schema;
use October\Rain\Database\Updates\Migration;

class BuilderTableCreateCiteContentFeatures extends Migration
{
    public function up()
    {
        if (!Schema::hasTable('nitm_content_features')) {
            Schema::create('nitm_content_features', function ($table) {
                $table->engine = 'InnoDB';
                $table->increments('id');
                $table->string('title', 140);
                $table->string('slug', 140);
                $table->string('description', 140);
                $table->integer('type_id')->nullable();
                $table->timestamp('created_at')->nullable();
                $table->integer('author_id')->nullable();
                $table->timestamp('updated_at')->nullable();
                $table->integer('editor_id')->nullable();
                $table->boolean('is_active')->default(0);
                $table->softDeletes();

                \Nitm\Content\Models\Category::create([
                    'title' => 'Feature Type',
                    'description' => 'Feature types',
                    'author_id' => 1,
                ]);
            });
        }
    }

    public function down()
    {
        //   Schema::dropIfExists('nitm_content_features');
    }
}
