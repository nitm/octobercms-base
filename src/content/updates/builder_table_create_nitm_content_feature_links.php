<?php

namespace Nitm\Content\Updates;

use Schema;
use October\Rain\Database\Updates\Migration;

class BuilderTableCreateCiteContentFeatureLinks extends Migration
{
    public function up()
    {
        if (!Schema::hasTable('nitm_content_feature_links')) {
            Schema::create('nitm_content_feature_links', function ($table) {
                $table->engine = 'InnoDB';
                $table->increments('id');
                $table->string('remote_type', 128);
                $table->text('remote_class');
                $table->string('remote_table', 128);
                $table->integer('remote_id');
                $table->integer('feature_id');
                $table->softDeletes();
            });
        }
    }

    public function down()
    {
        //   Schema::dropIfExists('nitm_content_feature_links');
    }
}
