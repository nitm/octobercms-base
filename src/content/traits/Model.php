<?php

namespace Nitm\Content\Traits;

use Nitm\Content\Models\BaseContent;
use Illuminate\Support\Str;

/**
 * Traits for Nitm Content MOdel.
 */
trait Model
{
    public static $withChildren = true;
    public static $forApi = false;
    public $eagerLoad;

    protected $_is;

    protected static $_ignoreAttribute = '__ignore__';

    public static function getIgnore()
    {
        return static::$_ignoreAttribute;
    }

    public static function setForApi($value)
    {
        static::$forApi = $value;
    }

    public static function isForApi()
    {
        return static::$forApi === true;
    }

    protected static function boot()
    {
        parent::boot();
        static::localBoot();
    }

    protected static function localBoot()
    {
        if (method_exists(static::class, 'extend')) {
            static::extend(function ($model) {
                // $model->bindEvent('model.beforeSetAttribute', function ($key, $value) use ($model) {
                //     /*
                //     * Set a given attribute on the model.
                //     * This is a customized version that allows for using mutators/preparers before setting the relation value.
                //     */
                //    if ($model->hasPrepareMutator($key)) {
                //        $method = 'prepare'.Str::studly($key).'Attribute';

                //        $value = $model->{$method}($value);
                //    }

                //     return $value;
                // });
            });
        }
    }

    //
    // Setters
    //

    /**
     * @inherit doc
     * @method setAttribute
     * @param  [type]       $key   [description]
     * @param  [type]       $value [description]
     */
    public function setAttribute($key, $value)
    {
        /*
       * Set a given attribute on the model.
       * This is a customized version that allows for using mutators/preparers before setting the relation value.
       */
        if ($this->hasPrepareMutator($key)) {
            //    echo "There is a prepare mutatory for $key\n";
            $method = 'prepare'.Str::studly($key).'Attribute';

            $value = $this->{$method}($value);
            //    echo "Called prepepare mutator for $key\n";
        }

        if ($value !== static::$_ignoreAttribute) {
            return parent::setAttribute($key, $value);
        }
    }

    public function prepareImageAttribute($image)
    {
        if ($image instanceof \Symfony\Component\HttpFoundation\File\UploadedFile) {
            return $image;
        }
        return static::$_ignoreAttribute;
    }

    public function prepareImagesAttribute($images)
    {
        $ret_val = [];
        foreach ((array)$images as $image) {
            if ($image instanceof \Symfony\Component\HttpFoundation\File\UploadedFile) {
                $ret_val [] = $image;
            }
        }
        return count($ret_val) ? $ret_val : static::$_ignoreAttribute;
    }

    /**
     * Determine if a set mutator exists for an attribute.
     *
     * @param string $key
     *
     * @return bool
     */
    public function hasPrepareMutator($key)
    {
        return method_exists($this, 'prepare'.Str::studly($key).'Attribute');
    }

    public function getIsAttribute()
    {
        if (!isset($this->_is)) {
            $this->_is = \Nitm\Content\Classes\ModelHelper::getIs($this);
        }

        return $this->_is;
    }

    public function getShouldCacheAttribute()
    {
        return property_exists($this, 'shouldCache') ? $this->shouldCache : true;
    }

    public function getPublicIdAttribute()
    {
        return $this->id;
        if (isset($this->attributes['slug'])) {
            return implode('-', [
               $this->slug,
               md5($this->id),
           ]);
        } else {
            return implode('-', [
               str_slug($this->title()),
               md5($this->id),
           ]);
        }
    }

    protected function bootDefaultRelations()
    {
        foreach (['hasOne', 'hasMany', 'attachOne', 'belongsToMany', 'attachMany', 'belongsTo'] as $relation) {
            if (property_exists($this, $relation.'Default')) {
                $this->{$relation} = array_replace($this->{$relation.'Default'}, $this->{$relation});
            }
        }
    }

    public function title()
    {
        try {
            return $this->title;
        } catch (\Exception $e) {
            return $this->id;
        }
    }

    /**
     * Get the action for this model.
     *
     * @return string the action
     */
    public function getAction()
    {
        return '';
    }

    /**
     * Get the table columns for a particular model.
     *
     * @method getTableColumns
     *
     * @return [type] [description]
     */
    public function getTableColumns()
    {
        $manager = $this->getConnection()->getDoctrineSchemaManager();
        $columns = $manager->listTableColumns($this->getTable());

        return $columns;
    }

    /**
     * Does the specified columnet exist?
     *
     * @param string $column
     *
     * @return boolean
     */
    public function hasColumn(string $column): bool
    {
        return array_key_exists($column, $this->getTableColumns());
    }

    /**
     * Get a nested array for select element.
     *
     * @param string $getter   The method to use to retrieve the data
     * @param string $labelKey THe key to use as the label value
     *
     * @return array The nested options
     */
    public function getNestedDropdownOptions($getter, $valueKey = 'title', $labelKey = 'id')
    {
        return $this->$getter(true)->listsNested($valueKey, $labelKey);
    }

    /**
     * Get a nested array for select element.
     *
     * @param string $getter   The method to use to retrieve the data
     * @param string $labelKey THe key to use as the label value
     *
     * @return array The nested options
     */
    public function getDropdownOptions($getter, $valueKey = 'title', $labelKey = 'id')
    {
        return $this->$getter(true)->lists($valueKey, $labelKey);
    }

    /**
     * Get options for dropdown with parent and children.
     *
     * @param string $class        THe Class we're using to create the list
     * @param bool   $getQuery     Do we want the query object or just the results?
     * @param bool   $withChildren Should we get the child nodes as well?
     *
     * @return Query|Collection The result
     */
    public function getRootWithChildren($class = null, $getQuery = false)
    {
        $class = $class ?: get_called_class();
        $query = static::resolveQuery($class);
        $query->allChildren(true);

        return $getQuery ? $query : $query->get();
    }

    /**
     * Get options for dropdown with parent and children.
     *
     * @param string $class    THe Class we're using to create the list
     * @param bool   $getQuery Do we want the query object or just the results?
     *
     * @return Query|Collection The result
     */
    public static function getWithChildren($class = null, $getQuery = false)
    {
        $class = $class ?: get_called_class();
        $query = static::resolveQuery($class);
        $query->allChildren();

        return $getQuery ? $query : $query->get();
    }

    /**
     * Get options for dropdown with parent and children.
     *
     * @param string $class    THe Class we're using to create the list
     * @param bool   $getQuery Do we want the query object or just the results?
     *
     * @return Query|Collection The result
     */
    public static function getWithAllChildren($class = null, $getQuery = false, $depth = 1)
    {
        $class = $class ?: get_called_class();
        $query = static::resolveQuery($class);
        $with = implode('.', array_fill(0, $depth, 'children'));
        $query->allChildren();

        return $getQuery ? $query : $query->get();
    }

    /**
     * Get the nested children.
     *
     * @param string $class The class to get children for
     *
     * @return TreeCollection [description]
     */
    protected function getNestedChildrenCollection($class = null)
    {
        $class = $class ?: get_called_class();
        $parent = $class::self()->first();
        if ($parent) {
            return $parent->getAllChildren()->toNested(false);
        }

        return [];
    }

    /**
     * Get the nested children.
     *
     * @param string $class The class to get children for
     *
     * @return TreeCollection [description]
     */
    protected static function getChildrenCollection($class = null, $depth = 2, $with = [])
    {
        $class = $class ?: get_called_class();
        if (static::$withChildren) {
            $with = array_merge($with, (array) implode('.', array_fill(0, $depth, 'children')));
        }
        $query = $class::with($with);
        $baseModel = $query->getModel();
        if (!$baseModel->exists) {
            $parent = $baseModel->with($with)->first();
        } else {
            $parent = $baseModel;
        }
        if ($parent && static::$withChildren) {
            return $parent->children;
        } elseif ($parent) {
            return $parent;
        } else {
            return new \October\Rain\Support\Collection;
        }
    }

    protected static function resolveClass($type)
    {
        $namespace = '\Nitm\Content\Models\\';
        $class = $namespace.studly_case($type);
        if (class_exists($class)) {
            return $class;
        }

        return null;
    }

    /**
     * Resolve the right model class.
     *
     * @param string $class [description]
     *
     * @return Model the resolved model
     */
    protected static function resolveModel($class = null)
    {
        $class = $class ?: get_called_class();
        if ($class && class_exists($class)) {
            $baseModel = new $class;
            $parentQuery = $baseModel->newQuery()->withoutGlobalScope(\October\Rain\Database\NestedTreeScope::class);
            if ($parentQuery->getModel()->slug === $baseModel->is) {
                $model = $parentQuery->getModel();
            } else {
                $parentQuery->where('slug', $baseModel->is);
                $model = $parentQuery->first();
            }
        } else {
            $model = $this;
        }

        return $model;
    }

    /**
     * Find the right model and compose the query.
     *
     * @param string $class   [description]
     * @param array  $options [description]
     *
     * @return \Illuminate\Database\Eloquent\Builder The query builder
     */
    protected static function resolveQuery($class, $options = [])
    {
        $model = static::resolveModel($class);
        if (!$model) {
            throw new \Exception("Unable to resolve database model for $class. You may need to create the entries for $class in the database");
        }
        $query = $model->newQuery();
        $query->setModel($model);
        $options = array_merge([
        ], $options);
        foreach ($options as $method => $option) {
            if (method_exists($query, $method)) {
                $query->$method($option);
            }
        }

        return $query;
    }

    public function scopeBase($query, $and = false)
    {
        $method = $and ? 'where' : 'orWhere';

        return $query->$method('slug', 'base-category');
    }

    /**
     * Primarily for json attributes. Make sure we get an array value.
     *
     * @param string $attribute The attribute name
     *
     * @return array The attribute
     */
    public function attributeToArray($attribute)
    {
        if (is_string($this->attributes[$attribute])) {
            return json_decode($this->attributes[$attribute], true);
        }

        return $this->attributes[$attribute];
    }

    public function toArray($isForApi = false)
    {
        $attributes = parent::toArray();
        if ($isForApi === true || BaseContent::isForApi() || static::isForApi()) {
            $attributes['id'] = $this->publicId;
        }

        return $attributes;
    }

    public function attributesToArray()
    {
        $attributes = parent::attributesToArray();
        if (BaseContent::isForApi() || static::isForApi()) {
            $attributes['id'] = $this->publicId;
        }

        return $attributes;
    }

    public function toFeedArray()
    {
        $attributes = array_intersect_key($this->toArray(), array_flip($this->visible));
        $attributes['id'] = $this->publicId;

        return $attributes;
    }

    /**
     * Get the count value for this model.
     *
     * @return int [description]
     */
    public function getCount()
    {
        return (int) $this->count;
    }

    /**
     * Get the value of a count relation.
     *
     * @param string $relation The relation name without the Count. i.e.: 'art' will look for the 'artCount relation'
     *
     * @return int [description]
     */
    public function getRelationCount($relation)
    {
        $relation = $relation.'Count';
        $relation = array_get($this->relations, $relation);
        if (isset($relation)) {
            return (int) $relation->count;
        } else {
            return 0;
        }
    }

    /**
     * Custom find function for use with the API.
     *
     * @param [type] $id            [description]
     * @param [type] $columns       [description]
     * @param array  $stringColumns The string columns that can be used to search by id
     * @param string $idColumn      The id column to use
     *
     * @return [type] [description]
     */
    public static function apiFind($id, $options = [])
    {
        $options['for'] = 'single';

        return static::internalApiFind($id, $options);
    }

    /**
     * Custom find function for use with the API.
     *
     * @param [type] $id      [description]
     * @param array  $options Options for building the query
     *
     * @return [type] [description]
     */
    protected static function internalApiFind($id, $options = [])
    {
        $query = static::apiQuery($options);

        static::parseOptions($query, $options);
        $query->whereMd5Id($id, $options);

        //   if (\App::environment() != 'dev') {
        //       $query->remember(5);
        //   }

        return isset($returnQuery) ? $query : $query->first();
    }

    /**
     * Query based on a numeric, string or hashed ID attribute
     * @method scopeWhereMd5Id
     * @param  Builder          $query         [description]
     * @param  integer|string          $id            [description]
     * @param  array          options  Options for the query. Int he format:
     * [
     *   hashedId => true|false,
     *   stringColumns => [],
     *   columns => [],
     *   idColumn => string,
     *   operator => string
     * ]
     * @return void                         [description]
     */
    public static function scopeWhereMd5Id($query, $id, $options=[])
    {
        extract($options);
        $hashedId = isset($hashedId) ? $hashedId : false;
        $columns = isset($columns) ? $columns : ['*'];
        $idColumn = isset($idColumn) ? $idColumn : 'id';
        $stringColumns = isset($stringColumns) ? $stringColumns : [];
        $operator = isset($negate) && $negate ? '!=' : '=';

        if (!is_numeric($id) && is_string($id) && !$hashedId) {
            $query->where(function ($query) use ($id, $stringColumns, $operator) {
                foreach ($stringColumns as $column) {
                    $query->orWhere($column, $operator, $id);
                }
            });
        } elseif ($hashedId && !is_numeric($id)) {
            if (!count($stringColumns)) {
                $query->whereRaw("MD5($idColumn::text) $operator ?", [static::getMd5Id($id)]);
            } else {
                $query->where(function ($query) use ($id, $stringColumns, $idColumn, $operator) {
                    $query->orWhereRaw("MD5($idColumn::text) $operator ? ", [static::getMd5Id($id)]);
                    foreach ($stringColumns as $column) {
                        $query->orWhere($column, $operator, $id);
                    }
                });
            }
        } else {
            $query->where($idColumn, $operator, $id);
        }
    }

    protected static function getMd5Id($id)
    {
        $ret_val = $id;
        $id = explode('-', $id);
        if (count($id) > 1 && static::isValidMd5(end($id))) {
            $ret_val = end($id);
        } elseif (is_numeric(end($id))) {
            $ret_val = md5(end($id));
        }

        return $ret_val;
    }

    protected static function getSlugFromMd5Id($id)
    {
        $id = explode('-', $id);
        if (count($id) > 1 && static::isValidMd5(end($id))) {
            array_pop($id);
        }

        return implode('-', $id);
    }

    public static function isValidMd5($md5 ='')
    {
        return strlen($md5) == 32 && ctype_xdigit($md5);
    }

    protected function setSingleCategoryAttributeValue($attribute, $class, $value)
    {
        $value = is_array($value) ? array_pop($value) : $value;
        if (!is_numeric($value)) {
            $category = $class::apiFind($value, ['hashedId' => true]);
            $this->attributes[$attribute] = $category ? $category->id : null;
        } else {
            $this->attributes[$attribute] = $value;
        }
    }

    protected static function parseOptions($query, $options = [])
    {
        extract($options);
        $with = isset($with) ? (array) $with : [];
        $for = isset($for) ? $for : null;
        switch ($for) {
          case 'feed':
          //The feed should load the minimum required information;
          static::addWithToQuery($query, $query->getModel()->with);
          break;

          case 'single':
          //We want to load everythign for a singlular model
          $with = array_merge((array) $query->getModel()->eagerWith, (array) $query->getModel()->with, (array) $with);
          static::addWithToQuery($query, array_unique($with));
          break;

          default:
          //We want to load only what's neccesary when loading multiple models
          $with = array_merge($query->getModel()->with, (array) $with);
          static::addWithToQuery($query, array_unique($with));
          break;
       }

        if (($filter = array_get($options, 'filter', false)) !== false) {
            $query->filter($filter);
        }

        foreach (array_except($options, ['filter']) as $method => $params) {
            if (method_exists($query, $method)) {
                call_user_func_array([$query, $method], $params);
            }
        }

        return $query;
    }

    /**
     * Custom API query function.
     *
     * @param array   $options  Array of parameters for the query builder
     * @param bool    $multiple Is this a request for multiple records?
     * @param Builder $query    The query to use
     *
     * @return [type] [description]
     */
    public static function apiQuery($options = [], $multiple = false, $query = null)
    {
        $query = $query ?: static::query();

        static::parseOptions($query, $options);

        return $query;
    }

    /**
     * Add relations to get with the query.
     *
     * @param Bulder $query    [description]
     * @param array  $mustHave [description]
     */
    protected static function addWithToQuery($query, $mustHave = [])
    {
        if ($query->getModel() && $query->getModel()->with) {
            $supportedWith = array_merge((array) $query->getModel()->with, $mustHave);
            $with = \Input::get('with');
            if (is_array($with) && count($with)) {
                $with = array_intersect($supportedWith, (array) \Input::get('with'));
            } else {
                $with = $supportedWith;
            }
            $except = [
              'apiToken',
            ];
            $with = array_diff($with, $except);
            //For the models created after this request automatically adjust the required fields
            self::extend(function ($user) use ($query, $with, $except) {
                $user->visible = array_unique(array_except(array_merge($user->visible, $with), $except));
                $user->with = $with;
            });
            call_user_func_array([$query, 'with'], $with);
        }
    }

    public function appendImplements($implement = [])
    {
        $this->implement = array_unique(array_merge((array) $this->implement, (array) $implement));
    }


    public function appendWith($with = [])
    {
        $this->with = array_unique(array_merge((array) $this->with, (array) $with));
    }

    public function appendEagerWith($with = [])
    {
        $this->eagerLoad = array_unique(array_merge((array) $this->eagerLoad, (array) $with));
    }

    public function appendVisible($visible = [])
    {
        $this->visible = array_unique(array_merge((array) $this->with, (array) $this->visible, (array) $visible));
    }

    public function appendFillable($fillable = [])
    {
        $this->fillable = array_unique(array_merge((array) $this->fillable, (array) $fillable));
    }

    public function appendAppends($appends = [])
    {
        $this->appends = array_unique(array_merge((array) $this->appends, (array) $appends));
    }

    public function appendHidden($hidden = [])
    {
        $this->hidden = array_unique(array_merge((array) $this->hidden, (array) $hidden));
    }

    public function getSessionKey()
    {
        if (!isset($this->sessionKey)) {
            $this->sessionKey = uniqid('session_key', true);
        }
        return $this->sessionKey;
    }

    public function saveRelation($relation, $value, $method='save')
    {
        if ($this->exists) {
            $this->$relation()->$method($value);
        } else {
            $this->bindEvent('model.afterCreate', function () use ($relation, $value, $method) {
                $this->$relation()->$method($value);
            });
        }
    }

    /**
     * Special situation during ember dev. Kept sending CSV arrays whenr evisitng form page to create new content.
     *
     * @return [type] [description]
     */
    protected function dedupe()
    {
        foreach ($this->attributes as $attribute => $value) {
            if (is_string($value)) {
                $this->attributes[$attribute] = explode(',', $value)[0];
            }
        }
    }

    /**
     * This supports extensions without including the parent extensions.
     *
     * @method localExtendableConstruct
     *
     * @return [type] [description]
     */
    public function localExtendableConstruct()
    {

        /*
         * Apply extensions
         */
        if (!$this->implement) {
            return;
        }
        if (is_string($this->implement)) {
            $uses = explode(',', $this->implement);
        } elseif (is_array($this->implement)) {
            $uses = $this->implement;
        } else {
            throw new Exception(sprintf('Class %s contains an invalid $implement value', get_class($this)));
        }
        foreach ($uses as $use) {
            $useClass = str_replace('.', '\\', trim($use));
            /*
             * Soft implement
             */
            if (substr($useClass, 0, 1) == '@') {
                $useClass = substr($useClass, 1);
                if (!class_exists($useClass)) {
                    continue;
                }
            }
            $this->extendClassWith($useClass);
        }
    }
}
