<?php namespace Nitm\Content\Components;

use Cms\Classes\ComponentBase;
use Nitm\Content\Models\Post as PostModel;
use Rainlab\Blog\Models\Category as PostCategory;

class Posts extends \Nitm\Content\Components\Paginated
{
    protected $posts;

    public function componentDetails()
    {
        return [
            'name'        => 'Blog List',
            'description' => 'No description provided yet...'
        ];
    }

    public function defineProperties()
    {
        return array_merge(parent::defineProperties(), [
            'defaultAuthor' => [
                'title'             => 'Default Post Author',
                'description'       => 'The default post author if no author is provided',
                'default'           => 'App',
                'type'              => 'string',
            ],
            'postModel' => [
                'title'             => 'Post Model Class',
                'description'       => 'The default model class for posts',
                'default'           => PostModel::class,
                'type'              => 'string',
            ],
            'category' => [
                'title'             => 'Posts Category',
                'description'       => 'The category of posts to show',
                'type'              => 'string',
            ],
            'layout' => [
                'title'             => 'Posts Layout',
                'description'       => 'The layout for the Posts list',
                'default'           => 'grid',
                'type'              => 'dropdown',
                'placeholder'       => 'Select layout...',
                'options'           => ['feed' => 'Feed', 'grid' => 'Grid', 'slides' => 'Slides']
            ],
        ]);
    }

    public function filters()
    {
        return [
            'categories' => \Cache::remember('post-categories', 30, function () {
                return PostCategory::all()->toArray();
            }),
        ];
    }

    public function posts()
    {
        $class = $this->property('postModel');
        if (!class_exists($class)) {
            $cclass = PostModel::class;
        }
        if (!isset($this->posts)) {
            $query = $class::apiQuery(array_merge([
               'with' => ['user']
           ]))->isPublished()
           ->orderBy('published_at', 'desc');
            $query->search(['s' => $this->filter('s'), 'filter' => array_filter((array)$this->filter(), function ($filter) {
                return strpos($filter, 'Select') === false;
            })], true);
            $this->items = $query->paginate($this->property('maxItems'), ['*'], 'page', input('page', 1));
            $this->posts = $this->items->getCollection();
        }

        return $this->posts;
    }

    public function setPosts($posts)
    {
        $this->posts = $posts;
    }
}
