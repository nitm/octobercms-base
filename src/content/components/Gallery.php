<?php namespace Nitm\Content\Components;

use Cms\Classes\ComponentBase;

class Gallery extends ComponentBase
{
    public function componentDetails()
    {
        return [
            'name'        => 'Gallery Component',
            'description' => 'Nitm Gallery component'
        ];
    }

    public function defineProperties()
    {
        return [
            'images' => [
                'title' => 'Images',
                'required' => true
            ],
            'settings' => [
                'title' => 'Settings',
                'required' => true
            ],
            'height' => [
                'title' => 'Height',
                'default' => '500px'
            ],
            'withOverlay' => [
                'title' => 'Show Overlay?',
                'default' => false,
                'type' => 'boolean'
            ],
            'showViewText' => [
                'title' => 'Show View Text?',
                'default' => true,
                'type' => 'boolean'
            ],
            'viewText' => [
                'title' => 'View Text',
                'default' => 'View',
                'type' => 'text'
            ]
        ];
    }

    public function images()
    {
        $images = $this->property('images');
        if ($images instanceof \Illuminate\Database\Eloquent\Collection) {
            return $images->toArray();
        } else {
            return array_map(function ($image) {
                return is_array($image) ? $image : $image->toArray();
            }, (array)$images);
        }
    }

    public function onRun()
    {
        $this->addCss('//cdnjs.cloudflare.com/ajax/libs/lightbox2/2.9.0/css/lightbox.min.css');
        $this->addJs('//cdnjs.cloudflare.com/ajax/libs/lightbox2/2.9.0/js/lightbox.min.js');
        $this->addJs('//cdnjs.cloudflare.com/ajax/libs/respond.js/1.4.2/respond.min.js');
    }
}
