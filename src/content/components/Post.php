<?php namespace App\Content\Components;

use Cms\Classes\ComponentBase;
use App\Content\Models\Post as PostModel;

class Post extends ComponentBase
{
    protected $_post;

    public function init()
    {
        parent::init();
        $galleryComponent = 'App\Content\Components\Gallery';
        $this->page->addComponent($galleryComponent, 'gallery', [
            'component' => $this,
            'images' => $this->post()->allImages,
            'height' => $this->property('galleryHeight'),
            'withOverlay' => false
        ]);

        $this->page->title = $this->post()->title();
    }

    public function componentDetails()
    {
        return [
            'name'        => 'Post Component',
            'description' => 'Single post'
        ];
    }

    public function defineProperties()
    {
        return [
            'galleryHeight' => [
                'title' => 'Gallery Height',
                'default' => '500px'
            ]
        ];
    }

    public function post()
    {
        if (!isset($this->_post)) {
            $this->_post = PostModel::apiFind($this->property('id'), [
                'with' => ['client', 'images', 'image']
            ]);
        }
        
        return $this->_post;
    }

    public function navigation()
    {
        return [
            'next' => $this->next(),
            'previous' => $this->previous()
        ];
    }
    
    public function next()
    {
        $next = $this->post() ? $this->post()->next() : null;
        return $next ? $next->toArray() : [];
    }
    
    public function previous()
    {
        $next = $this->post() ? $this->post()->previous() : null;
        return $next ? $next->toArray() : [];
    }
}
