<?php

namespace Nitm\Content\Components;

class MoreButton extends \Cms\Classes\ComponentBase
{
    protected $users;
    protected $profile;

    /**
     * @return [type]
     */
    public function componentDetails()
    {
        return [
            'name' => 'MoreButton',
            'description' => 'Displays a more buton'
        ];
    }

    public function defineProperties()
    {
        return [
            'component' => [
                'title'             => 'Component',
                'description'       => 'The component',
            ],
            'action' => [
                'title'             => 'Action',
                'description'       => 'The action to run when more is clicked',
                'default'           => 'onMore',
            ],
            'containerId' => [
                'title'             => 'Container ID',
                'description'       => 'The container ID for the parent container',
                'default'           => 'item-list',
            ],
            'text' => [
                'title'             => 'Text',
                'description'       => 'The text for the button',
                'default'           => 'Show more',
            ],
            'buttonClass' => [
                'title'             => 'Button Class',
                'description'       => 'The css class for the button',
                'default'           => 'btn btn--lazy-load',
            ],
            'buttonSvg' => [
                'title'             => 'Button SVG',
                'description'       => 'The svg html for the button',
                'default'           => '<svg class="icon icon--lazy-load"><use xlink:href="#icon--lazy-load"></use></svg>',
            ],
            'appendMore' => [
                'title'             => 'Append Data?',
                'description'       => 'Should data be appended?',
                'default'           => false,
                'type'              => 'boolean'
            ],
        ];
    }

    public function component()
    {
        return $this->property('component');
    }

    public function appendMore()
    {
        return $this->property('appendMore') ? 1 : 0;
    }

    public function containerId()
    {
        $id = $this->property('containerId');
        return in_array(substr($id, 0, 1), ['#', '.', '[']) ? $id : '#' . $id;
    }

    public function action()
    {
        return $this->property('action') ?: 'onMore';
    }

    public function text()
    {
        return $this->property('text');
    }

    public function buttonClass()
    {
        return $this->property('buttonClass');
    }

    public function buttonSvg()
    {
        return $this->property('buttonSvg');
    }
}