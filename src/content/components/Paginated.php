<?php

namespace Nitm\Content\Components;

use Rainlab\User\Models\User;

abstract class Paginated extends \Cms\Classes\ComponentBase
{
    /**
     * The current collection
     * @var LengthAwarePaginator | SimplePaginator
     */
    protected $items;

    /**
     * The pagination
     * @var array
     */
    protected $pagination;

    /**
     * The user profile
     * @var User
     */
    protected $profile;

    public function init()
    {
        parent::init();
    }

    public function defineProperties()
    {
        return [
            'maxItems' => [
                'title'             => 'Max items',
                'description'       => 'The most amount of items allowed',
                'default'           => 10,
                'type'              => 'string',
                'validationPattern' => '^[0-9]+$',
                'validationMessage' => 'The Max items property can contain only numeric symbols'
            ],
            'paginationStyle' => [
                'title'             => 'Pagination styles',
                'description'       => 'The pagination style',
                'default'           => 'single',
                'type'              => 'dropdown',
                'validationMessage' => 'The Max items property can contain only numeric symbols',
                'options'           => ['single' => 'Single More Button', 'all' => 'Page Links']
            ],
            'paginationButtonClass' => [
                'title'             => 'Button Class',
                'description'       => 'The css class for the button',
                'default'           => 'btn btn--lazy-load btn-info',
            ],
            'showNavigation' => [
                'title'             => 'Show Navigation?',
                'description'       => 'Should the navigation be shown?',
                'type'              => 'checkbox',
            ],
            'profile' => [
                'title'             => 'Profile',
                'description'       => 'The user profile',
            ],
            'appendMore' => [
                'title'             => 'Append Data?',
                'description'       => 'Should data be appended?',
                'default'           => true,
                'type'              => 'boolean'
            ]
        ];
    }

    public function pagination()
    {
        if (!$this->pagination) {
            $this->pagination = \Nitm\Content\Classes\CollectionHelper::getPagination($this->items);
        }
        return $this->pagination;
    }

    public function onInit() {
        $paginationComponent = $this->property('paginationStyle') == 'single' ? MoreButton::class : Pagination::class;
        $this->page->addComponent($paginationComponent, 'moreButton'.class_basename(get_class($this)), [
            'component' => $this,
            'buttonClass' => $this->property('paginationButtonClass')
        ]);
    }

    public function onMore() {
        $id = $this->containerId();
        $hash = in_array(strpos($id, 0), ['#', '.','[']) ? '' : '#';
        $selector = ($this->property('appendMore') ? '@' : '').$hash;
        return [
            $selector.$id => $this->renderPartial('@default')
        ];
    }

    public function onFilter()
    {
        return $this->onMore();
    }

    public function profile($id=null)
    {
        if (!isset($this->profile)) {
            if ($id || !$this->property('profile')) {
                $id = $id ?: (\Auth::getUser() ? \Auth::getUser()->id : null);
                if ($id) {
                    $this->profile = \Nitm\Content\Models\User::apiFind($id);
                } else {
                    $this->profile = new User;
                }
            } else {
                $this->profile = $this->property('profile');
            }
        }
        return $this->profile;
    }

    /**
     * Get the replacement container for ajax load Child classes can override this
     */
    public function containerId()
    {
        return (strtolower(class_basename(get_called_class()))).($this->profile() ? $this->profile()->id : uniqid());
    }

    public function filter($key=null, $getPlaceholder=false)
    {
        if($key) {
            $fullKey = $key ? 'filter.'.$key : 'filter';
            $filter = $this->page->param($key, array_get($_REQUEST, $fullKey) ?? "") ?? "";
            return !$filter && $key && $getPlaceholder ? 'Select '.ucwords($key) : $filter;
        } else {
            return "Select ...";
        }
    }

    public function filters()
    {
        return [];
    }

    public function selected()
    {
        return [];
    }

    public function generateUrl($url, $options=[], $mergeQueryString=true)
    {
        $options = $mergeQueryString ? array_merge($options, $_GET) : $options;
        return url($url, $options);
    }


    public function getSortOptions() {
        return [
            'id' => [
                'label' => 'Created',
                'url' => $this->getUrl([
                    'sort' => 'id'
                ])
            ],
        ];
    }

    public function getOrderOptions() {
        return [
            'desc' => 'Descending',
            'asc' => 'Ascending'
        ];
    }

    public function getUrl($options=[], $path=null) {
        $parts = parse_url(\Url::current());
        $path = $path ?: $this->baseUrl ?: $parts['path'];
        $args = array_merge($_GET, $options);
        return $parts['path'].'?'.http_build_query($args);
    }

    public function sort() {
        return \Input::all('sort') ?: $this->property('sort');
    }

    public function order() {
        return \Input::all('order') ?: $this->property('order');
    }
}
