<?php

namespace Nitm\Content\Classes;

/**
 * This class provides configuration helper functions for config variables.
 *
 * @author malcolm@bklyn.co
 */
class QueryHelper
{
    /**
     * Supports mysql and postgres json field searching
     * @param  [type] $field [description]
     * @param  [type] $where [description]
     * @return [type]        [description]
     */
    public static function searchJsonField($where, $inclusive=false)
    {
        $conditions = [];
        $xor = $inclusive ? ' OR ' : ' AND ';
        switch (\Db::connection()->getDriverName()) {
            case 'mysql':
            foreach ($where as $field=>$params) {
                $conditions[] = 'JSON_CONTAINS('.$field.', \''.json_encode($params).'\')';
            }
            break;

            case 'pgsql':
            foreach ($where as $field=>$params) {
                $conditions[] = $field.'::jsonb @> \''.json_encode($params).'\'::jsonb';
            }
            break;
        }
        return implode($xor, $conditions);
    }
}
